import { Component, OnInit } from '@angular/core';
import { loginSuccess } from './store/actions/login.action';
import { AuthState } from './store/reducers/login.reducer';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  // title = 'my-angular14-app';
  userId: string | null = null;
  role: string | null = null;
  constructor(private store: Store<{ auth: AuthState }>) {}
  ngOnInit(): void {
    const storedUserId = localStorage.getItem('userid');
    const storedRole = localStorage.getItem('role');
    const token = localStorage.getItem('token');
    // If user ID is found in local storage, dispatch loginSuccess action
    if (token && storedUserId && storedRole) {
      this.userId = storedUserId;
      this.role = storedRole;
      this.store.dispatch(
        loginSuccess({ userId: this.userId, role: this.role })
      );
    }
  }
}

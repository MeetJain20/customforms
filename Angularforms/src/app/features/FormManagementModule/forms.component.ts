import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormsState } from './store/reducers/form.reducer';
import { Subscription } from 'rxjs';
import { getLoading } from './store/selectors/forms.selector';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  loadingSubscription: Subscription | undefined;

  constructor(private store: Store<{ forms: FormsState }>) {}

  ngOnInit(): void {
    // Subscribe to the loading state from the store
    this.loadingSubscription = this.store.pipe(select(getLoading)).subscribe((loading: boolean) => {
      this.isLoading = loading;
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from the loading state subscription
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }
}

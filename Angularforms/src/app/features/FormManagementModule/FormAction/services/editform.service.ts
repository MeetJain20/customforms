import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MAIN_URL } from 'src/app/constants/constant';
import { FormData as FD, FieldType } from '../../types/forms';
import { CustomResponse } from '../type/customresponse';
import { ResponseData } from '../type/responsedata';
import { ResponseType } from '../components/display-form/types/response.type';
import { MessageService } from 'primeng/api';
@Injectable({
  providedIn: 'root',
})
export class EditformService {
  constructor(private http: HttpClient,private messageService:MessageService) {}

  fetchCurrentFormDetails(formid: string): Observable<FD[]> {
    return this.http
      .get<FD[]>(`${MAIN_URL}/form/getcurrentform/${formid}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }

  fetchCurrentFormResponses(formid: string): Observable<CustomResponse> {
    return this.http
      .get<ResponseData[]>(`${MAIN_URL}/empform/getresponses/${formid}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        map((response)=>{
          return { responseData: response }
        }),
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }
  updateFormTitle(formId: string, formtitle: string): Observable<FD> {
    return this.http
      .put<FD>(`${MAIN_URL}/form/updateformtitle`, {
        formid: formId,
        formtitle: formtitle,
      })
      .pipe(
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }

  updateFormDesc(formId: string, formdesc: string): Observable<FD> {
    return this.http
      .put<FD>(`${MAIN_URL}/form/updateformdesc`, {
        formid: formId,
        formdesc: formdesc,
      })
      .pipe(
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }


  saveFormField(formId: string, fieldData: FieldType): Observable<FD> {
    if (!fieldData.question.trim()) {
      this.messageService.add({ severity: 'warn', summary: 'Warn', detail: 'Question field cannot be empty' });
      return throwError('Question field is empty');
    }
  
    return this.http
      .put<FD>(`${MAIN_URL}/form/updateformfields`, { formid: formId, fielddata: fieldData })
      .pipe(
        tap(() => {
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Field saved successfully!' });
        }),
        catchError((error) => {
          throw new Error(error.message);
        })
        );
      }
      
      
      addFormField(formId: string, newFieldData: FieldType): Observable<FD> {
        return this.http
        .put<FD>(`${MAIN_URL}/form/addnewfield`, {
          formid: formId,
          newFieldData: newFieldData,
        })
        .pipe(
          tap(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Success',
              detail: 'Field Added Successfully',
            });  }),
          catchError((error) => {
            this.messageService.add({
              severity: 'error',
              summary: 'Error',
              detail: 'Error Adding New Field!',
            });
            throw new Error(error.message);
          })
          );
        }
        
        deleteFormField(formId: string, fieldId: string): Observable<{fieldid:string}> {
          return this.http
          .delete<FD>(
            `${MAIN_URL}/form/deletefield?formid=${formId}&fieldid=${fieldId}`
            )
            .pipe(
              tap(() => {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Success',
                  detail: 'Field Deleted Successfully',
                });              }),
              map((response: FD) => {
          return { fieldid: fieldId };
        }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Deleting Field!',
          });
          throw new Error(error.message);
        })
      );
  }

  useTemplateForm(formId: string, adminId: string): Observable<FD> {
    return this.http
      .post<FD>(`${MAIN_URL}/form/createFromTemplate`, {
        formid: formId,
        adminId: adminId,
      })
      .pipe(
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }

  saveFormResponse(formId: string, adminId: string,employeeId:string,responses:ResponseType[]): Observable<{message: string}> {
    return this.http
      .post<{message: string}>(`${MAIN_URL}/empform/saveresponse`, {
        formId: formId,
        adminId: adminId,
        employeeId,
        responses
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Response Submitted Successfully',
          });  }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Submitting Response!',
          });
          throw new Error(error.message);
        })
      );
  }

  saveCurrentForm(formId: string): Observable<FD> {
    return this.http
      .put<FD>(`${MAIN_URL}/form/updateformstatus`, {
        formid: formId,
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Form Saved Successfully',
          });  }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Saving Form!',
          });
          throw new Error(error.message);
        })
      );
  }

  editCurrentForm(formId: string): Observable<FD> {
    return this.http
      .put<FD>(`${MAIN_URL}/form/updateeditstatus`, {
        formid: formId,
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'info',
            summary: 'Info',
            detail: 'Now you can edit the form!',
          });  }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Making Form Editable!',
          });
          throw new Error(error.message);
        })
      );
  }

  saveCurrentFormAsTemplate(formId: string): Observable<FD> {
    return this.http
      .put<FD>(`${MAIN_URL}/form/updatetemplatestatus`, {
        formid: formId,
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Template Saved Successfully',
          });  }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Saving Template!',
          });
          throw new Error(error.message);
        })
      );
  }
  uploadFileToS3(newFormData: FormData): Observable<{link:string}> {
  return this.http
    .post<{link:string}>(`${MAIN_URL}/file/uploadFileToS3`, newFormData) // Send newFormData directly
    .pipe(
      map((response) => {
        return { link: response.link };
      }),
      catchError((error) => {
        throw new Error(error.message);
      })
    );
}
}

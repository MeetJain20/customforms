// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { uploadFile,uploadFileFailure,uploadFileSuccess } from '../actions/uploadfiletos3.action';

@Injectable()
export class UploadFileEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService
      ) {}
      
      uploadFile$ = createEffect(() => this.actions$.pipe(
    ofType(uploadFile),
    mergeMap(({ newFormData }) => this.editformService.uploadFileToS3(newFormData)
      .pipe(
        map(({link}) => uploadFileSuccess({link})),
        catchError(error => of(uploadFileFailure({ error })))
      ))
    )
  );

  
}

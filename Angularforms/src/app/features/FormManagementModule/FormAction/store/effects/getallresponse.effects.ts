// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { getFormResponse,getFormResponseFailure,getFormResponseSuccess } from '../actions/getallresponse.action';

@Injectable()
export class GetFormResponseEffects {
  constructor(
    private actions$: Actions,
    private editFormsService: EditformService
  ) {}

  getFormResponse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFormResponse),
      mergeMap(({formid}) =>
        this.editFormsService.fetchCurrentFormResponses(formid).pipe(
          map((responseData) =>
          getFormResponseSuccess({
             responseData:responseData.responseData
            })
          ),
          catchError((error) =>
            of(getFormResponseFailure({ error: error.message }))
          )
        )
      )
    )
  );
}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, debounceTime, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';

import {
  updateFormDesc,
  updateFormDescFailure,
  updateFormDescSuccess,
} from '../actions/updateformdesc.action';
import { updateFormTitle, updateFormTitleFailure, updateFormTitleSuccess } from '../actions/updateformtitle.action';

@Injectable()
export class UpdateTitleDescEffects {
  constructor(
    private actions$: Actions,
    private editformService: EditformService
  ) {}

  updateFormTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateFormTitle),
      debounceTime(500),
      mergeMap(({ formId, formtitle }) =>
        this.editformService.updateFormTitle(formId, formtitle).pipe(
          map((response) => updateFormTitleSuccess({formtitle:response.formtitle})),
          catchError((error) => of(updateFormTitleFailure({ error })))
        )
      )
    )
  );

  updateFormDesc$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateFormDesc),
      debounceTime(500),
      mergeMap(({ formId, formdesc }) =>
        this.editformService.updateFormDesc(formId, formdesc).pipe(
          map((response) => updateFormDescSuccess({formdesc:response.formdesc})),
          catchError((error) => of(updateFormDescFailure({ error })))
        )
      )
    )
  );
}

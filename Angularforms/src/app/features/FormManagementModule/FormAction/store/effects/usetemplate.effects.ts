// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { useTemplate,useTemplateFailure,useTemplateSuccess } from '../actions/usetemplate.action';
import { Router } from '@angular/router';

@Injectable()
export class UseTemplateEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService,
        private router:Router
      ) {}
      
  useTemplate$ = createEffect(() => this.actions$.pipe(
    ofType(useTemplate),
    mergeMap(({ formId, adminId }) => this.editformService.useTemplateForm(formId, adminId)
      .pipe(map((response) => {
        this.router.navigateByUrl(`actions/createform/${response._id}`);
          return useTemplateSuccess();
        }),
        
        catchError(error => of(useTemplateFailure({ error })))
      ))
    )
  );

  
}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { EditformService } from '../../services/editform.service';
import { saveForm,saveFormFailure,saveFormSuccess } from '../actions/saveform.action';
import { Router } from '@angular/router';
import { editStatusForm,editStatusFormFailure,editStatusFormSuccess } from '../actions/changeformstatustoedit.action';
@Injectable()
export class SaveEditFormEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService,
        private router:Router
      ) {}

      saveForm$ = createEffect(() => this.actions$.pipe(
        ofType(saveForm),
        mergeMap(({ formId }) => this.editformService.saveCurrentForm(formId)
          .pipe(
            map(() => {
            this.router.navigateByUrl('/forms/admindashboard');
              return saveFormSuccess();
            }),
            catchError(error => of(saveFormFailure({ error })))
          ))
        )
      );

      editStatusForm$ = createEffect(() => this.actions$.pipe(
        ofType(editStatusForm),
        mergeMap(({ formId }) => this.editformService.editCurrentForm(formId)
          .pipe(
            map(() => {
            this.router.navigateByUrl(`/actions/createform/${formId}`);
              return editStatusFormSuccess();
            }),
            catchError(error => of(editStatusFormFailure({ error })))
          ))
        )
      );


}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { deleteFormField,deleteFormFieldFailure,deleteFormFieldSuccess } from '../actions/deletefield.action';

@Injectable()
export class DeleteFieldEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService
      ) {}
      
      deleteFormField$ = createEffect(() => this.actions$.pipe(
    ofType(deleteFormField),
    mergeMap(({ formId, fieldId }) => this.editformService.deleteFormField(formId, fieldId)
      .pipe(
        map(({fieldid}) => deleteFormFieldSuccess({fieldid})),
        catchError(error => of(deleteFormFieldFailure({ error })))
      ))
    )
  );

  
}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import {
  getFormDetails,
  getFormDetailsFailure,
  getFormDetailsSuccess,
} from '../actions/getformdetails.action';

@Injectable()
export class GetFormEffects {
  constructor(
    private actions$: Actions,
    private editFormsService: EditformService
  ) {}

  getFormDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFormDetails),
      switchMap((action) =>
        this.editFormsService.fetchCurrentFormDetails(action.formid).pipe(
          map((response) =>
            getFormDetailsSuccess({
                adminId: response[0].adminId,
              formtitle: response[0].formtitle,
              formdesc: response[0].formdesc,
              fields: response[0].fields,
              isComplete: response[0].isComplete,
              isTemplate: response[0].isTemplate,
            })
          ),
          catchError((error) =>
            of(getFormDetailsFailure({ error: error.message }))
          )
        )
      )
    )
  );
}

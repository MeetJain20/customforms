// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { saveFormField,saveFormFieldFailure,saveFormFieldSuccess } from '../actions/savefield.action';

@Injectable()
export class SaveFieldEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService
      ) {}
      
  saveFormField$ = createEffect(() => this.actions$.pipe(
    ofType(saveFormField),
    mergeMap(({ formId, fieldData }) => this.editformService.saveFormField(formId, fieldData)
      .pipe(
        map((response) => saveFormFieldSuccess({fields:response.fields})),
        catchError(error => of(saveFormFieldFailure({ error })))
      ))
    )
  );

  
}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import {
  saveFormResponse,
  saveFormResponseFailure,
  saveFormResponseSuccess,
} from '../actions/saveresponse.action';
import { Router } from '@angular/router';

@Injectable()
export class SaveFormResponseEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private editformService: EditformService
  ) {}

  saveFormResponse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(saveFormResponse),
      mergeMap(({ formId, adminId, employeeId, responses }) =>
        this.editformService
          .saveFormResponse(formId, adminId, employeeId, responses)
          .pipe(
            map(() => {
              this.router.navigateByUrl('/forms/employeedashboard');
              return saveFormResponseSuccess();
            }),
            catchError((error) => of(saveFormResponseFailure({ error })))
          )
      )
    )
  );
}

// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EditformService } from '../../services/editform.service';
import { addFormField,addFormFieldFailure,addFormFieldSuccess } from '../actions/addnewfield.action';

@Injectable()
export class AddNewFieldEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService
      ) {}
      
  addFormField$ = createEffect(() => this.actions$.pipe(
    ofType(addFormField),
    mergeMap(({ formId, newFieldData }) => this.editformService.addFormField(formId, newFieldData)
      .pipe(
        map((response) => addFormFieldSuccess({fields:response.fields})),
        catchError(error => of(addFormFieldFailure({ error })))
      ))
    )
  );

  
}

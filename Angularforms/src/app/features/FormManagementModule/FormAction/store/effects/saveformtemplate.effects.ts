// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { EditformService } from '../../services/editform.service';
import { saveFormTemplate,saveFormTemplateFailure,saveFormTemplateSuccess } from '../actions/saveformTemplate.action';
import { Router } from '@angular/router';
@Injectable()
export class SaveFormTemplateEffects {
    constructor(
        private actions$: Actions,
        private editformService: EditformService,
        private router:Router
      ) {}

      saveFormTemplate$ = createEffect(() => this.actions$.pipe(
        ofType(saveFormTemplate),
        mergeMap(({ formId }) => this.editformService.saveCurrentFormAsTemplate(formId)
          .pipe(
            map(() => {
            this.router.navigateByUrl('/forms/admindashboard');
              return saveFormTemplateSuccess();
            }),
            catchError(error => of(saveFormTemplateFailure({ error })))
          ))
        )
      );


}

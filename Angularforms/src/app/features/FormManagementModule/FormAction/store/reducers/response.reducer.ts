import { createReducer, on } from '@ngrx/store';
import { responseUpdate } from '../actions/response.action';
import { ResponseType } from '../../components/display-form/types/response.type';
import { logoutSuccess } from 'src/app/store/actions/login.action';

export interface ResponseState {
  fieldResponse: ResponseType[];
}

export const initialState: ResponseState = {
  fieldResponse: []
};

export const responseReducer = createReducer(
  initialState,
  on(responseUpdate, (state, { responseData }) => {
    const existingIndex = state.fieldResponse.findIndex(entry => entry.fieldid === responseData.fieldid);

    if (existingIndex !== -1) {
      const updatedFieldResponse = [...state.fieldResponse];
      updatedFieldResponse[existingIndex] = { ...updatedFieldResponse[existingIndex], response: responseData.response };
      return {
        ...state,
        fieldResponse: updatedFieldResponse
      };
    } else {
      return {
        ...state,
        fieldResponse: [
          ...state.fieldResponse,
          {
            fieldid: responseData.fieldid,
            question: responseData.question,
            type: responseData.type,
            response: responseData.response
          }
        ]
      };
    }
  }),

  on(logoutSuccess, ()=>initialState)

);

// form.reducer.ts
import { createReducer, on } from '@ngrx/store';
import { saveFormResponse,saveFormResponseFailure,saveFormResponseSuccess } from '../actions/saveresponse.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';

export interface SaveResponseState {
  loading: boolean;
  link:string,
  error: string;
}

export const initialState: SaveResponseState = {
  loading: false,
  link:"",
  error: '',
};

export const saveResponseReducer = createReducer(
  initialState,
  on(saveFormResponse, (state) => ({
    ...state,
    loading: true,
  })),
  on(saveFormResponseSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(saveFormResponseFailure, (state,{error}) => ({
    ...state,
    error,
    loading: false,
  })),

  on(logoutSuccess, ()=>initialState)

)
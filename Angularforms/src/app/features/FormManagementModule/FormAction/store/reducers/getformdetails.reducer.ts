// form.reducer.ts
import { createReducer, on } from '@ngrx/store';
import {
  getFormDetails,
  getFormDetailsFailure,
  getFormDetailsSuccess,
} from '../actions/getformdetails.action';
import { addFirstField } from '../actions/addfirstfield.action';
import { editFieldType } from '../actions/edittypeoffield.action';
import {
  saveFormField,
  saveFormFieldFailure,
  saveFormFieldSuccess,
} from '../actions/savefield.action';
import {
  saveForm,
  saveFormFailure,
  saveFormSuccess,
} from '../actions/saveform.action';
import {
  saveFormTemplate,
  saveFormTemplateFailure,
  saveFormTemplateSuccess,
} from '../actions/saveformTemplate.action';
import {
  deleteFormField,
  deleteFormFieldFailure,
  deleteFormFieldSuccess,
} from '../actions/deletefield.action';
import {
  addFormField,
  addFormFieldFailure,
  addFormFieldSuccess,
} from '../actions/addnewfield.action';
import {
  updateFormTitle,
  updateFormTitleFailure,
  updateFormTitleSuccess,
} from '../actions/updateformtitle.action';
import {
  updateFormDesc,
  updateFormDescFailure,
  updateFormDescSuccess,
} from '../actions/updateformdesc.action';
import {
  editStatusForm,
  editStatusFormFailure,
  editStatusFormSuccess,
} from '../actions/changeformstatustoedit.action';
import {
  useTemplate,
  useTemplateFailure,
  useTemplateSuccess,
} from '../actions/usetemplate.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';
export interface FormDetailsState {
  loading: boolean;
  error: string;
  adminId: string;
  formtitle: string;
  formdesc: string;
  fields: any[];
  isComplete: boolean;
  isTemplate: boolean;
}

export const initialState: FormDetailsState = {
  loading: false,
  error: '',
  adminId: '',
  formtitle: '',
  formdesc: '',
  fields: [],
  isComplete: false,
  isTemplate: false,
};

export const getFormDetailsReducer = createReducer(
  initialState,
  on(getFormDetails, (state) => ({
    ...state,
    loading: true,
  })),
  on(
    getFormDetailsSuccess,
    (
      state,
      { adminId, formtitle, formdesc, fields, isComplete, isTemplate }
    ) => ({
      ...state,
      loading: false,
      adminId,
      formtitle,
      formdesc,
      fields: [...fields],
      isComplete,
      isTemplate,
    })
  ),
  on(getFormDetailsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error: error,
  })),

  on(addFirstField, (state, { id, typee }) => ({
    ...state,
    fields: [...state.fields, { fieldid: id, type: typee }],
  })),

  on(editFieldType, (state, { id, typee }) => ({
    ...state,
    fields: state.fields.map((field) =>
      field.fieldid === id ? { ...field, type: typee } : field
    ),
  })),

  on(updateFormTitle, (state) => ({ ...state })),
  on(updateFormTitleSuccess, (state, { formtitle }) => ({
    ...state,
    formtitle,
  })),
  on(updateFormTitleFailure, (state, { error }) => ({
    ...state,
    error,
  })),

  on(updateFormDesc, (state) => ({ ...state })),
  on(updateFormDescSuccess, (state, { formdesc }) => ({
    ...state,
    formdesc,
  })),
  on(updateFormDescFailure, (state, { error }) => ({
    ...state,
    error,
  })),

  on(saveFormField, (state) => ({ ...state, loading: true })),
  on(saveFormFieldSuccess, (state, { fields }) => {
    const changedField = fields.find((updatedField) => {
      const existingField = state.fields.find(
        (field) => field.fieldid === updatedField.fieldid
      );
      return (
        existingField &&
        (existingField?.placeholder !== updatedField?.placeholder ||
          existingField.question !== updatedField.question ||
          existingField.isrequired !== updatedField.isrequired ||
          !arraysEqual(existingField?.options, updatedField?.options))
      );
    });

    if (changedField) {
      const updatedFields = state.fields.map((field) =>
        field.fieldid === changedField.fieldid
          ? { ...field, ...changedField }
          : field
      );

      return {
        ...state,
        fields: updatedFields,
        loading: false,
      };
    }

    return state;
  }),
  on(saveFormFieldFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(useTemplate, (state) => ({ ...state, loading: true })),
  on(useTemplateSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(useTemplateFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(addFormField, (state) => ({ ...state })),
  on(addFormFieldSuccess, (state, { fields }) => {
    const newField = fields.find(
      (field) =>
        !state.fields.some(
          (existingField) => existingField.fieldid === field.fieldid
        )
    );
    return {
      ...state,
      fields: [...state.fields, newField],
    };
  }),
  on(addFormFieldFailure, (state, { error }) => ({
    ...state,
    error,
  })),

  on(deleteFormField, (state) => ({ ...state })),
  on(deleteFormFieldSuccess, (state, { fieldid }) => ({
    ...state,
    fields: state.fields.filter((field) => field.fieldid !== fieldid),
  })),
  on(deleteFormFieldFailure, (state, { error }) => ({
    ...state,
    error,
  })),

  on(saveForm, (state) => ({ ...state, loading: true })),
  on(saveFormSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(saveFormFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(editStatusForm, (state) => ({ ...state, loading: true })),
  on(editStatusFormSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(editStatusFormFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(saveFormTemplate, (state) => ({ ...state, loading: true })),
  on(saveFormTemplateSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(saveFormTemplateFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(logoutSuccess, ()=>initialState)

);

function arraysEqual(arr1: any[], arr2: any[]): boolean {
  if (arr1 === arr2) return true;
  if (arr1 == null || arr2 == null) return false;
  if (arr1.length !== arr2.length) return false;

  for (let i = 0; i < arr1.length; ++i) {
    if (arr1[i] !== arr2[i]) return false;
  }

  return true;
}

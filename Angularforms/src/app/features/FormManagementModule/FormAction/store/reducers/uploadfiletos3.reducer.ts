import { createReducer, on } from '@ngrx/store';
import { uploadFile,uploadFileFailure,uploadFileSuccess } from '../actions/uploadfiletos3.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';

export interface FileState {
  loading: boolean;
  link:string,
  error: string;
}

export const initialState: FileState = {
  loading: false,
  link:"",
  error: '',
};

export const uploadFileReducer = createReducer(
  initialState,
  on(uploadFile, (state) => ({
    ...state,
    loading: true,
  })),
  on(uploadFileSuccess, (state,{link}) => ({
    ...state,
    link,
    loading: false,
  })),
  on(uploadFileFailure, (state,{error}) => ({
    ...state,
    error,
    loading: false,
  })),

  on(logoutSuccess, ()=>initialState)

)
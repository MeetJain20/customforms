// form.reducer.ts
import { createReducer, on } from '@ngrx/store';
import { getFormResponse,getFormResponseFailure,getFormResponseSuccess } from '../actions/getallresponse.action';
import { ResponseData } from '../../type/responsedata';
import { logoutSuccess } from 'src/app/store/actions/login.action';

export interface AllResponseState {
  loading: boolean;
  responseData:ResponseData[],
  error: string;
}

export const initialState: AllResponseState = {
  loading: false,
  responseData:[],
  error: '',
};

export const getAllResponseReducer = createReducer(
  initialState,
  on(getFormResponse, (state) => ({
    ...state,
    loading: true,
  })),
  on(getFormResponseSuccess, (state,{responseData}) => ({
    ...state,
    responseData,
    loading: false,
  })),
  on(getFormResponseFailure, (state,{error}) => ({
    ...state,
    error,
    loading: false,
  })),

  on(logoutSuccess, ()=>initialState)

)
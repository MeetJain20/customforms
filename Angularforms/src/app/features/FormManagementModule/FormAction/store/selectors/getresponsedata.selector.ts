import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ResponseState } from '../reducers/response.reducer';
import { AllResponseState } from '../reducers/getallresponse.reducer';

export const allResponseDataState = createFeatureSelector<AllResponseState>('allresponse');

export const getAllResponse = createSelector(
    allResponseDataState,
  (state: AllResponseState) => state.responseData
);
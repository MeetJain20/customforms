import { createSelector, createFeatureSelector } from '@ngrx/store';
import { FormDetailsState } from '../reducers/getformdetails.reducer';
import { ResponseState } from '../reducers/response.reducer';

export const responseState = createFeatureSelector<ResponseState>('response');

export const updateResponse = createSelector(
    responseState,
  (state: ResponseState) => state.fieldResponse
);
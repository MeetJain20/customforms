import { createSelector, createFeatureSelector } from '@ngrx/store';
import { FileState } from '../reducers/uploadfiletos3.reducer';
export const fileState = createFeatureSelector<FileState>('upload');

export const uploadFileToResponse = createSelector(
    fileState,
  (state: FileState) => state.link 
);
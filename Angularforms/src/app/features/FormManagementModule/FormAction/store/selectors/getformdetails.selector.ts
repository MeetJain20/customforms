import { createSelector, createFeatureSelector } from '@ngrx/store';
import { FormDetailsState } from '../reducers/getformdetails.reducer';

export const currentFormDetailsState = createFeatureSelector<FormDetailsState>('getform');

export const getCurrentFormTitle = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.formtitle 
);
export const getCurrentFormDesc = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.formdesc 
);
export const getCurrentFormFields = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.fields 
);
export const getCurrentFormIsComplete = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.isComplete 
);
export const getCurrentFormIsTemplate = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.isTemplate 
);
export const getCurrentFormAdminId = createSelector(
    currentFormDetailsState,
  (state: FormDetailsState) => state.adminId 
);
export const getLoading = createSelector(
  currentFormDetailsState,
  (state: FormDetailsState) => state.loading // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
import { createAction, props } from '@ngrx/store';

export const deleteFormField = createAction(
  '[Delete Field] Delete Field',
  props<{ formId: string, fieldId: string }>()
);

export const deleteFormFieldSuccess = createAction(
  '[Delete Field] Delete Field Success',
  props<{ fieldid: string }>()
);

export const deleteFormFieldFailure = createAction(
  '[Delete Field] Delete Field Failure',
  props<{ error: string }>()
);
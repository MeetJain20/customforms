import { createAction, props } from '@ngrx/store';

export const editFieldType = createAction(
  '[Edit Type of Field] Edit Field Type',
  props<{ id: string,typee:string }>()
);

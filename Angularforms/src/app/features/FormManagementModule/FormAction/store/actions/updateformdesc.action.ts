import { createAction, props } from '@ngrx/store';

export const updateFormDesc = createAction(
  '[Update Desc] Update Form Desc',
  props<{ formId: string, formdesc: string }>()
);

export const updateFormDescSuccess = createAction(
  '[Update Desc] Update Form Desc Success',
  props<{ formdesc: string }>()

);

export const updateFormDescFailure = createAction(
  '[Update Desc] Update Form Desc Failure',
  props<{ error: string }>()
);
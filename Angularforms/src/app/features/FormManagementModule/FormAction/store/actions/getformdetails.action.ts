import { createAction, props } from '@ngrx/store';

export const getFormDetails = createAction(
  '[Get Form] Get Form',
  props<{ formid: string }>()
);

export const getFormDetailsSuccess = createAction(
  '[Get Form] Get Form Success',
  props<{ adminId:string,formtitle:string,formdesc:string,fields:any[],isComplete:boolean,isTemplate:boolean }>()
);

export const getFormDetailsFailure = createAction(
  '[Get Form] Get Form Failure',
  props<{ error: string }>()
);
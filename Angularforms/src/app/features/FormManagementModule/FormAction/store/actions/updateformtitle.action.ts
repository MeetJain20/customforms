import { createAction, props } from '@ngrx/store';

export const updateFormTitle = createAction(
  '[Update Title] Update Form Title',
  props<{ formId: string, formtitle: string }>()
);

export const updateFormTitleSuccess = createAction(
  '[Update Title] Update Form Title Success',
  props<{ formtitle: string }>()

);

export const updateFormTitleFailure = createAction(
  '[Update Title] Update Form Title Failure',
  props<{ error: string }>()
);
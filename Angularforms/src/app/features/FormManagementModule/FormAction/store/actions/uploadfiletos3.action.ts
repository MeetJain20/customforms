import { createAction, props } from '@ngrx/store';

export const uploadFile = createAction(
  '[Upload File] Upload File',
  props<{ newFormData: FormData }>()
);

export const uploadFileSuccess = createAction(
  '[Upload File] Upload File Success',
  props<{link: string}>()
);

export const uploadFileFailure = createAction(
  '[Upload File] Upload File Failure',
  props<{ error: string }>()
);
import { createAction, props } from '@ngrx/store';

export const saveForm = createAction(
  '[Save Form] Save Form',
  props<{ formId: string }>()
);

export const saveFormSuccess = createAction(
  '[Save Form] Save Form Success'
);

export const saveFormFailure = createAction(
  '[Save Form] Save Form Failure',
  props<{ error: string }>()
);
import { createAction, props } from '@ngrx/store';

export const editStatusForm = createAction(
  '[Edit Form Again] Edit Form Again',
  props<{ formId: string }>()
);

export const editStatusFormSuccess = createAction(
  '[Edit Form Again] Edit Form Again Success'
);

export const editStatusFormFailure = createAction(
  '[Edit Form Again] Edit Form Again Failure',
  props<{ error: string }>()
);
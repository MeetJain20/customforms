import { createAction, props } from '@ngrx/store';
import { ResponseType } from '../../components/display-form/types/response.type';

export const responseUpdate = createAction('[Response Handler] Response Update',
props<{ responseData: ResponseType }>());
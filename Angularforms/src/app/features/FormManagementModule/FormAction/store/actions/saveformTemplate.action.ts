import { createAction, props } from '@ngrx/store';

export const saveFormTemplate = createAction(
  '[Save Form Template] Save Form As Template',
  props<{ formId: string }>()
);

export const saveFormTemplateSuccess = createAction(
  '[Save Form Template] Save Form As Template Success'
);

export const saveFormTemplateFailure = createAction(
  '[Save Form Template] Save Form As Template Failure',
  props<{ error: string }>()
);
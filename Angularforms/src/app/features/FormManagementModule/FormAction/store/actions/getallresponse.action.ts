import { createAction, props } from '@ngrx/store';
import { ResponseData } from '../../type/responsedata';

export const getFormResponse = createAction(
  '[Get Form Response] Get Form Response',
  props<{ formid: string }>()
);

export const getFormResponseSuccess = createAction(
  '[Get Form Response] Get Form Response Success',
  props<{ responseData:ResponseData[] }>()
);

export const getFormResponseFailure = createAction(
  '[Get Form Response] Get Form Response Failure',
  props<{ error: string }>()
);
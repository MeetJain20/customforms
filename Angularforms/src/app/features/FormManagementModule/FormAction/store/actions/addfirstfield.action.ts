import { createAction, props } from '@ngrx/store';

export const addFirstField = createAction(
  '[Add First Field] First Field Form',
  props<{ id: string,typee:string }>()
);

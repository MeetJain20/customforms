import { createAction, props } from '@ngrx/store';

export const useTemplate = createAction(
  '[Use Template] Use Template',
  props<{ formId: string, adminId: string }>()
);

export const useTemplateSuccess = createAction(
  '[Use Template] Use Template Success',
);

export const useTemplateFailure = createAction(
  '[Use Template] Use Template Failure',
  props<{ error: string }>()
);
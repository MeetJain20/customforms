import { createAction, props } from '@ngrx/store';
import { FieldType } from '../../../types/forms';

export const addFormField = createAction(
  '[Add Field] Add Field',
  props<{ formId: string, newFieldData: FieldType }>()
);

export const addFormFieldSuccess = createAction(
  '[Add Field] Add Field Success',
  props<{ fields: FieldType[] }>()
);

export const addFormFieldFailure = createAction(
  '[Add Field] Add Field Failure',
  props<{ error: string }>()
);
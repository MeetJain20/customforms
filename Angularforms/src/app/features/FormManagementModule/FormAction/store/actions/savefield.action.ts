import { createAction, props } from '@ngrx/store';
import { FieldType } from '../../../types/forms';

export const saveFormField = createAction(
  '[Save Field] Save Field',
  props<{ formId: string; fieldData: FieldType }>()
);

export const saveFormFieldSuccess = createAction(
  '[Save Field] Save Field Success',
  props<{ fields: any[] }>()
);

export const saveFormFieldFailure = createAction(
  '[Save Field] Save Field Failure',
  props<{ error: string }>()
);

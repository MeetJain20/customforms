import { createAction, props } from '@ngrx/store';
import { ResponseType } from '../../components/display-form/types/response.type';

export const saveFormResponse = createAction(
  '[Save Form Response] Save Form Response',
  props<{ formId: string,adminId:string,employeeId:string,responses:ResponseType[] }>()
);

export const saveFormResponseSuccess = createAction(
  '[Save Form Response] Save Form Response Success'
);

export const saveFormResponseFailure = createAction(
  '[Save Form Response] Save Form Response Failure',
  props<{ error: string }>()
);
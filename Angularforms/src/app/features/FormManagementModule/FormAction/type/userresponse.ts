
export interface UserResponse{
    fieldid: string,
    question: string,
    type: string,
    response: string | string[]
}
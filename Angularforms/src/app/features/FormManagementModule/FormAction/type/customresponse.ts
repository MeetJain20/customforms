import { ResponseData } from "./responsedata";

export interface CustomResponse{
    responseData: ResponseData[]
}
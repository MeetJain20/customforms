import { UserResponse } from "./userresponse";

export interface ResponseData{
    formId: string,
    employeeId: string,
    empName:string,
    adminId:string,
    responses: UserResponse[]
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormActionComponent } from './form-action.component';
import { DisplayFormComponent } from './components/display-form/display-form.component';
import { MakeFormComponent } from './components/make-form/make-form.component';
import { DisplayResponseComponent } from './components/display-form/display-response/display-response.component';

const routes: Routes = [
{
    path: '',
    component: FormActionComponent,
    children: [
      {
        path: 'createform/:id',
        component: MakeFormComponent,
      },
      {
        path: 'displayform/:id',
        component: DisplayFormComponent,
      },
      {
        path: 'viewresponse/:id',
        component: DisplayResponseComponent,
      }
    ]
 
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormActionRoutingModule {}

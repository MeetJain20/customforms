import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { editFieldType } from 'src/app/features/FormManagementModule/FormAction/store/actions/edittypeoffield.action';
import { FormDetailsState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/getformdetails.reducer';
import { OptionType } from 'src/app/features/FormManagementModule/FormAction/type/optiontype';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-new-fields',
  templateUrl: './new-fields.component.html',
  styleUrls: ['./new-fields.component.scss'],
})
export class NewFieldsComponent implements OnChanges {
  @Input() fieldData!: FieldType;
  currentdiv: boolean = false;
  optionList: OptionType[] = [
    { value: 'shortanswer', label: 'Short Answer' },
    { value: 'longanswer', label: 'Long Answer' },
    { value: 'multiplechoice', label: 'Multiple Choice' },
    { value: 'checkboxtype', label: 'Checkbox Type' },
    { value: 'dropdown', label: 'Dropdown' },
    { value: 'datetype', label: 'Date Type' },
    { value: 'timetype', label: 'Time Type' },
    { value: "filetype", label: "File Type" },
  ];

  selectedOption!: OptionType;

  constructor(private store: Store<{ getform: FormDetailsState }>) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['fieldData'] && changes['fieldData'].currentValue) {
      this.selectedOption = {
        value: this['fieldData'].type.replace(/\s+/g, '').toLowerCase(),
        label: this['fieldData'].type,
      };
    }
  }

  onOptionSelected(event: MouseEvent) {
    const updatedFieldData = { ...this.fieldData, type: this.selectedOption.label };
    this.store.dispatch(editFieldType({ id: this.fieldData.fieldid, typee: this.selectedOption.label }));
    this.fieldData = updatedFieldData;
  }

  showBorder() {
    this.currentdiv = true;
  }
}

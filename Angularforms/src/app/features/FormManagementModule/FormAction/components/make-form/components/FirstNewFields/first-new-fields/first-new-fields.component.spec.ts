import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNewFieldsComponent } from './first-new-fields.component';

describe('FirstNewFieldsComponent', () => {
  let component: FirstNewFieldsComponent;
  let fixture: ComponentFixture<FirstNewFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstNewFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FirstNewFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { saveFormField } from 'src/app/features/FormManagementModule/FormAction/store/actions/savefield.action';
import { FormDetailsState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/getformdetails.reducer';
import { MessageService } from 'primeng/api';
import { deleteFormField } from 'src/app/features/FormManagementModule/FormAction/store/actions/deletefield.action';
import {v4 as uuidv4} from "uuid";
import { addFormField } from 'src/app/features/FormManagementModule/FormAction/store/actions/addnewfield.action';
import { Subscription } from 'rxjs';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';
@Component({
  selector: 'app-functionalites',
  templateUrl: './functionalites.component.html',
  styleUrls: ['./functionalites.component.scss'],
  providers: [MessageService],
})
export class FunctionalitesComponent implements OnInit,OnDestroy {
  @Input() hasChanged!: boolean;
  @Input() fieldState!: FieldType;
  @Output() hasChangedChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  formId: string = '';
  isRequired!: boolean;
  newFieldData:FieldType = {
    fieldid: uuidv4(),
    type: "Short Answer",
    question: "",
    placeholder: "Enter the text you want the user to see here ...",
    isrequired:false,
  };
  private routeSubscription: Subscription | undefined;

  constructor(
    private store: Store<{ getform: FormDetailsState }>,
    private route: ActivatedRoute,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.routeSubscription = this.route.paramMap.subscribe((params) => {
      this.formId = params.get('id') || '';
    });
    this.isRequired = this.fieldState.isrequired;
  }

  ngOnDestroy(): void {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
  }
  
  updatefieldHandler() {
 
    this.store.dispatch(
      saveFormField({ formId: this.formId, fieldData: this.fieldState })
    );
    this.hasChanged = false;
    this.hasChangedChange.emit(this.hasChanged); // Emitting the hasChanged state
  }

  addFieldHandler(){
    this.store.dispatch(
      addFormField({ formId: this.formId, newFieldData: this.newFieldData })
    );
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail:"New Field Added!"
    });
    this.hasChanged = false;
    this.hasChangedChange.emit(this.hasChanged); 
  }

  deleteFieldHandler() {
    this.store.dispatch(
      deleteFormField({ formId: this.formId, fieldId: this.fieldState.fieldid })
    );
   
    this.hasChanged = false;
    this.hasChangedChange.emit(this.hasChanged); // Emitting the hasChanged state
  }

  changeIsRequired(event: any) {
    this.isRequired = event.target.checked;
    this.hasChanged = true;
    this.fieldState.isrequired = this.isRequired;
    this.hasChangedChange.emit(this.hasChanged); // Emitting the hasChanged state
  }
}

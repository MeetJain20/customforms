import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormDetailsState } from '../../store/reducers/getformdetails.reducer';
import { getFormDetails } from '../../store/actions/getformdetails.action';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  getCurrentFormDesc,
  getCurrentFormFields,
  getCurrentFormIsComplete,
  getCurrentFormIsTemplate,
  getCurrentFormTitle,
} from '../../store/selectors/getformdetails.selector';
import { saveForm } from '../../store/actions/saveform.action';
import { saveFormTemplate } from '../../store/actions/saveformTemplate.action';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { updateFormTitle } from '../../store/actions/updateformtitle.action';
import { updateFormDesc } from '../../store/actions/updateformdesc.action';
import { FieldType } from '../../../types/forms';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-make-form',
  templateUrl: './make-form.component.html',
  styleUrls: ['./make-form.component.scss'],
  providers: [MessageService],
})
export class MakeFormComponent implements OnInit, OnDestroy {
  formId!: string;
  fields: FieldType[] = [];
  isComplete: boolean = false;
  isTemplate: boolean = false;
  showSaveForm: boolean = false;
  showSaveFormAsTemplate: boolean = false;
  currentFormData: FormGroup;
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private store: Store<{ getform: FormDetailsState }>,
    private router: Router,
    private messageService: MessageService
  ) {
    this.currentFormData = new FormGroup({
      formtitle: new FormControl(''),
      formdesc: new FormControl(''),
      fields: new FormControl([]),
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.formId = params.get('id') || '';
    });
    this.store.dispatch(getFormDetails({ formid: this.formId }));
    this.store
      .pipe(select(getCurrentFormTitle), takeUntil(this.unsubscribe$))
      .subscribe((formtitle) => {
        this.currentFormData.get('formtitle')?.setValue(formtitle);
      });
    this.store
      .pipe(select(getCurrentFormDesc), takeUntil(this.unsubscribe$))
      .subscribe((formdesc) => {
        this.currentFormData.get('formdesc')?.setValue(formdesc);
      });
    this.store
      .pipe(select(getCurrentFormFields), takeUntil(this.unsubscribe$))
      .subscribe((fields) => {
        this.currentFormData.get('fields')?.setValue(fields);
        console.log(fields);
        this.fields = fields;
      });

    this.store
      .pipe(
        select(getCurrentFormIsComplete),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((isComplete) => {
        this.isComplete = isComplete;
        if (localStorage.getItem('role') === 'admin') {
          if (!isComplete) {
            this.showSaveForm = true;
          }
        }
      });
    this.store
      .pipe(
        select(getCurrentFormIsTemplate),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((isTemplate) => {
        this.isTemplate = isTemplate;
        if (localStorage.getItem('role') === 'admin') {
          if (!isTemplate) {
            this.showSaveFormAsTemplate = true;
          }
        }
      });

    this.currentFormData
      .get('formtitle')
      ?.valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.store.dispatch(
          updateFormTitle({ formId: this.formId, formtitle: value })
        );
      });

    this.currentFormData
      .get('formdesc')
      ?.valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.store.dispatch(
          updateFormDesc({ formId: this.formId, formdesc: value })
        );
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  saveFinalForm() {
    this.store.dispatch(saveForm({ formId: this.formId }));
  }

  saveFinalFormAsTemplate() {
    this.store.dispatch(saveFormTemplate({ formId: this.formId }));
  }

  noEmptySpacesValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    if (control.value && control.value.trim().length === 0) {
      return { emptySpaces: true };
    }
    return null;
  }
}

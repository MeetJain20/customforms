import { Component, Input, OnInit } from '@angular/core';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-field-type-attributes',
  templateUrl: './field-type-attributes.component.html',
  styleUrls: ['./field-type-attributes.component.scss']
})
export class FieldTypeAttributesComponent implements OnInit {
  @Input() fieldData!:FieldType;
  @Input() type!:string;

  constructor() { }

  ngOnInit(): void {
  }

}

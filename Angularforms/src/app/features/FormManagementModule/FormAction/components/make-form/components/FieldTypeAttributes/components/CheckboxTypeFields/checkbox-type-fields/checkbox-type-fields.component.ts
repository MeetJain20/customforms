import { Component, Input, OnInit } from '@angular/core';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-checkbox-type-fields',
  templateUrl: './checkbox-type-fields.component.html',
  styleUrls: ['./checkbox-type-fields.component.scss']
})
export class CheckboxTypeFieldsComponent implements OnInit {
  @Input() fieldData!:FieldType;
  fieldState: any;
  hasChanged: boolean = false;
  errorMessage: string = '';
  constructor() {}
  
  ngOnInit(): void {
    this.fieldState = {
      fieldid: this.fieldData.fieldid,
      type: this.fieldData.type || 'Multiple Choice',
      isrequired: this.fieldData.isrequired || false,
      question: this.fieldData.question || '',
      options: this.fieldData.options || [''],
    };
  }

  handleQuestionChange(event: Event){
    const target = event.target as HTMLInputElement;
    this.fieldState = { ...this.fieldState, question: target.value };
    this.hasChanged = true;
  }

  handleOptionChange(index: number, event: any) {
    const newOptions = [...this.fieldState.options];
    newOptions[index] = event.target.value;
    this.fieldState = { ...this.fieldState, options: newOptions };
    this.errorMessage = "";
    this.hasChanged = true;
    // console.log(this.fieldState);
  }

  handleAddOption() {
    if (
      this.fieldState.options[this.fieldState.options.length - 1].trim() !== ''
    ) {
      const newOptions = [...this.fieldState.options, ''];
      this.fieldState = { ...this.fieldState, options: newOptions };
      this.errorMessage = '';
      this.hasChanged = true;
    } else {
      this.errorMessage = 'Please enter a value for the option';
    }
  }

  handleDeleteOption(index: number) {
    const newOptions = this.fieldState.options.filter(
      (_: any, i: number) => i !== index
    );
    this.fieldState = { ...this.fieldState, options: newOptions };
    this.errorMessage = '';
    this.hasChanged = true; // Set flag when option is deleted
  }

  changeHandler(){
    this.hasChanged = true;
  }

  onHasChangedChange(hasChanged: boolean) {
    this.hasChanged = hasChanged;
  }

}

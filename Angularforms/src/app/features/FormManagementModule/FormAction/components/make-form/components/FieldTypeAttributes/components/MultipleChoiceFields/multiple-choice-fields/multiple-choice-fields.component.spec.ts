import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleChoiceFieldsComponent } from './multiple-choice-fields.component';

describe('MultipleChoiceFieldsComponent', () => {
  let component: MultipleChoiceFieldsComponent;
  let fixture: ComponentFixture<MultipleChoiceFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultipleChoiceFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MultipleChoiceFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

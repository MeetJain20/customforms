import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateTypeFieldsComponent } from './date-type-fields.component';

describe('DateTypeFieldsComponent', () => {
  let component: DateTypeFieldsComponent;
  let fixture: ComponentFixture<DateTypeFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateTypeFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DateTypeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

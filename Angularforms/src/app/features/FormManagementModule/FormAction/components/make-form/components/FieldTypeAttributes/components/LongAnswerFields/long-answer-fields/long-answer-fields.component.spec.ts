import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LongAnswerFieldsComponent } from './long-answer-fields.component';

describe('LongAnswerFieldsComponent', () => {
  let component: LongAnswerFieldsComponent;
  let fixture: ComponentFixture<LongAnswerFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LongAnswerFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LongAnswerFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

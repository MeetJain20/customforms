import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { addFirstField } from 'src/app/features/FormManagementModule/FormAction/store/actions/addfirstfield.action';
import { FormDetailsState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/getformdetails.reducer';
import { OptionType } from 'src/app/features/FormManagementModule/FormAction/type/optiontype';
import { v4 as uuidv4 } from 'uuid';
@Component({
  selector: 'app-first-new-fields',
  templateUrl: './first-new-fields.component.html',
  styleUrls: ['./first-new-fields.component.scss']
})
export class FirstNewFieldsComponent implements OnInit {

  optionList:OptionType[] = [
    { value: "shortanswer", label: "Short Answer" },
    { value: "longanswer", label: "Long Answer" },
    { value: "multiplechoice", label: "Multiple Choice" },
    { value: "checkboxtype", label: "Checkbox Type" },
    { value: "dropdown", label: "Dropdown" },
    { value: "datetype", label: "Date Type" },
    { value: "timetype", label: "Time Type" },
    { value: "filetype", label: "File Type" },
  ];

  selectedOption!:OptionType;

  constructor(private store: Store<{getform:FormDetailsState}>) { }

  ngOnInit(): void {
  }

  onOptionSelected(event: MouseEvent) {
    const newFieldId = uuidv4();
    this.store.dispatch(addFirstField({id:newFieldId, typee: this.selectedOption.label}))
  }

}

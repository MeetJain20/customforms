import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxTypeFieldsComponent } from './checkbox-type-fields.component';

describe('CheckboxTypeFieldsComponent', () => {
  let component: CheckboxTypeFieldsComponent;
  let fixture: ComponentFixture<CheckboxTypeFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxTypeFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CheckboxTypeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

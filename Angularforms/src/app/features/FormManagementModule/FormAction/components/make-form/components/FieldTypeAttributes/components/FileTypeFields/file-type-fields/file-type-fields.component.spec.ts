import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileTypeFieldsComponent } from './file-type-fields.component';

describe('FileTypeFieldsComponent', () => {
  let component: FileTypeFieldsComponent;
  let fixture: ComponentFixture<FileTypeFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileTypeFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FileTypeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

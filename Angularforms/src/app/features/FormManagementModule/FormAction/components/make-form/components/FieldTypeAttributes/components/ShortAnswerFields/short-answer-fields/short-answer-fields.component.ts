import { Component, Input, OnInit } from '@angular/core';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-short-answer-fields',
  templateUrl: './short-answer-fields.component.html',
  styleUrls: ['./short-answer-fields.component.scss']
})
export class ShortAnswerFieldsComponent implements OnInit {
  @Input() fieldData!:FieldType;
  fieldState!:FieldType;
  hasChanged:boolean=false;
  constructor() { }

  ngOnInit(): void {
    this.fieldState = {
      fieldid: this.fieldData.fieldid,
      type: this.fieldData.type || "Short Answer",
      isrequired: this.fieldData.isrequired || false,
      question: this.fieldData.question || "",
      placeholder: this.fieldData.placeholder || "Enter the text you want the user to see here ...",
    };
  }

  handleQuestionChange(event: Event){
    const target = event.target as HTMLInputElement;
    this.fieldState = { ...this.fieldState, question: target.value };
    this.hasChanged = true;
  }

  handlePlaceholderChange(event:Event){
    const target = event.target as HTMLInputElement;
    this.fieldState = { ...this.fieldState, placeholder: target.value };
    this.hasChanged = true;
  }

  onHasChangedChange(hasChanged: boolean) {
    this.hasChanged = hasChanged;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalitesComponent } from './functionalites.component';

describe('FunctionalitesComponent', () => {
  let component: FunctionalitesComponent;
  let fixture: ComponentFixture<FunctionalitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunctionalitesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FunctionalitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-file-type-fields',
  templateUrl: './file-type-fields.component.html',
  styleUrls: ['./file-type-fields.component.scss']
})
export class FileTypeFieldsComponent implements OnInit {
  @Input() fieldData!:FieldType;
  fieldState!:FieldType;
  hasChanged:boolean=false;
  constructor() { }

  ngOnInit(): void {
    this.fieldState = {
      fieldid: this.fieldData.fieldid,
      type: this.fieldData.type || "Date Type",
      isrequired: this.fieldData.isrequired || false,
      question: this.fieldData.question || "",
    };
  }

  handleQuestionChange(event: Event){
    const target = event.target as HTMLInputElement;
    this.fieldState = { ...this.fieldState, question: target.value };
    this.hasChanged = true;
  }

  onHasChangedChange(hasChanged: boolean) {
    this.hasChanged = hasChanged;
  }

}

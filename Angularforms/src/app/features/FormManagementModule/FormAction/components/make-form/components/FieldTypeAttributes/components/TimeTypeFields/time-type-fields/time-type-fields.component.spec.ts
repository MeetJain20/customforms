import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeTypeFieldsComponent } from './time-type-fields.component';

describe('TimeTypeFieldsComponent', () => {
  let component: TimeTypeFieldsComponent;
  let fixture: ComponentFixture<TimeTypeFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeTypeFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimeTypeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortAnswerFieldsComponent } from './short-answer-fields.component';

describe('ShortAnswerFieldsComponent', () => {
  let component: ShortAnswerFieldsComponent;
  let fixture: ComponentFixture<ShortAnswerFieldsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShortAnswerFieldsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShortAnswerFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

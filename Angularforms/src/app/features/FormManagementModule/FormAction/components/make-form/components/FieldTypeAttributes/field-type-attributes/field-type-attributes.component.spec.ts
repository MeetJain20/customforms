import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldTypeAttributesComponent } from './field-type-attributes.component';

describe('FieldTypeAttributesComponent', () => {
  let component: FieldTypeAttributesComponent;
  let fixture: ComponentFixture<FieldTypeAttributesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FieldTypeAttributesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FieldTypeAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

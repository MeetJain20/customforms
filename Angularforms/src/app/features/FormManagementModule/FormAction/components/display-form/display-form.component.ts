import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormDetailsState } from '../../store/reducers/getformdetails.reducer';
import { MessageService } from 'primeng/api';
import { getFormDetails } from '../../store/actions/getformdetails.action';
import { saveFormTemplate } from '../../store/actions/saveformTemplate.action';
import { editStatusForm } from '../../store/actions/changeformstatustoedit.action';
import { useTemplate } from '../../store/actions/usetemplate.action';
import { saveFormResponse } from '../../store/actions/saveresponse.action';
import { ResponseState } from '../../store/reducers/response.reducer';
import { updateResponse } from '../../store/selectors/response.selector';
import { SaveResponseState } from '../../store/reducers/saveresponse.reducer';
import { getFormResponse } from '../../store/actions/getallresponse.action';
import { AllResponseState } from '../../store/reducers/getallresponse.reducer';
import { getAllResponse } from '../../store/selectors/getresponsedata.selector';
import { getCurrentFormAdminId, getCurrentFormDesc, getCurrentFormFields, getCurrentFormIsComplete, getCurrentFormIsTemplate, getCurrentFormTitle } from '../../store/selectors/getformdetails.selector';
import { FieldType } from '../../../types/forms';
import { ResponseData } from '../../type/responsedata';
import { ResponseType } from './types/response.type';

@Component({
  selector: 'app-display-form',
  templateUrl: './display-form.component.html',
  styleUrls: ['./display-form.component.scss'],
  providers: [MessageService]
})
export class DisplayFormComponent implements OnInit, OnDestroy {

  formId!: string;
  fields: FieldType[] = [];
  isComplete: boolean = false;
  formAdminId: string = "";
  isTemplate: boolean = false;
  userid: string = localStorage.getItem('userid') || "";
  showSaveFormAsTemplate: boolean = false;
  isAdmin: boolean = localStorage.getItem('role') === 'admin';
  isEmployee: boolean = localStorage.getItem('role') === 'employee';
  formResponse!: ResponseType[];
  responseData!: ResponseData[];
  currentFormData: FormGroup;
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private store: Store<{ getform: FormDetailsState, response: ResponseState, saveresponse: SaveResponseState, allresponse: AllResponseState }>,
    private router: Router,
    private messageService: MessageService
  ) {
    this.currentFormData = new FormGroup({
      formtitle: new FormControl(''),
      formdesc: new FormControl(''),
      fields: new FormControl([]),
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.formId = params.get('id') || '';
    });
    this.store.dispatch(getFormDetails({ formid: this.formId }));
    this.store.dispatch(getFormResponse({ formid: this.formId }));

    this.store.pipe(select(getCurrentFormTitle), takeUntil(this.unsubscribe$)).subscribe((formtitle) => {
      this.currentFormData.get('formtitle')?.setValue(formtitle);
    });
    this.store.pipe(select(getCurrentFormDesc), takeUntil(this.unsubscribe$)).subscribe((formdesc) => {
      this.currentFormData.get('formdesc')?.setValue(formdesc);
    });
    this.store.pipe(select(getCurrentFormFields), takeUntil(this.unsubscribe$)).subscribe((fields) => {
      this.currentFormData.get('fields')?.setValue(fields);
      this.fields = fields;
    });

    this.store.pipe(select(getCurrentFormIsComplete), takeUntil(this.unsubscribe$)).subscribe((isComplete) => {
      this.isComplete = isComplete;
    });
    this.store.pipe(select(getCurrentFormIsTemplate), takeUntil(this.unsubscribe$)).subscribe((isTemplate) => {
      this.isTemplate = isTemplate;
      if (localStorage.getItem('role') === 'admin') {
        if (!isTemplate) {
          this.showSaveFormAsTemplate = true;
        }
      }
    });
    this.store.pipe(select(getCurrentFormAdminId), takeUntil(this.unsubscribe$)).subscribe((adminId) => {
      this.formAdminId = adminId;
    });

    this.store.pipe(select(updateResponse), takeUntil(this.unsubscribe$)).subscribe((fieldResponse) => {
      this.formResponse = [...fieldResponse];
    });
    this.store.pipe(select(getAllResponse), takeUntil(this.unsubscribe$)).subscribe((responseData) => {
      this.responseData = [...responseData];
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  viewResponseHandler(): void {
    this.router.navigate([`actions/viewresponse/${this.formId}`], { queryParams: { totalResponseData: JSON.stringify(this.responseData) } });
  }

  useTemplateHandler(): void {
    this.store.dispatch(useTemplate({ formId: this.formId, adminId: this.userid }))
  }

  editFormHandler(): void {
    this.store.dispatch(editStatusForm({ formId: this.formId }));
  }

  submitResponseHandler(): void {
    this.store.dispatch(saveFormResponse({ formId: this.formId, adminId: this.formAdminId, employeeId: this.userid, responses: this.formResponse }));
  }
  saveFinalFormAsTemplate() {
    const hasEmptyQuestion = this.fields.some(
      (field) => field.question.trim() === ''
    );

    if (hasEmptyQuestion) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warn',
        detail:
          'Some fields have empty questions. Please fill them before saving.',
      });

      return;
    }
    this.store.dispatch(saveFormTemplate({ formId: this.formId }));
  }

}

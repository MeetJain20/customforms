export interface ResponseType {
  fieldid: string;
  question: string;
  type: string;
  response: string | string[];
}

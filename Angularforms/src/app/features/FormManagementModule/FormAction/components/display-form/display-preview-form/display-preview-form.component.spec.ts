import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPreviewFormComponent } from './display-preview-form.component';

describe('DisplayPreviewFormComponent', () => {
  let component: DisplayPreviewFormComponent;
  let fixture: ComponentFixture<DisplayPreviewFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayPreviewFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayPreviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject, debounceTime, distinctUntilChanged } from 'rxjs';
import { responseUpdate } from 'src/app/features/FormManagementModule/FormAction/store/actions/response.action';
import { ResponseState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/response.reducer';
import { OptionType } from 'src/app/features/FormManagementModule/FormAction/type/optiontype';
import { ResponseType } from '../../../../../types/response.type';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

export interface OptionData{
  value: string,
  label:string
}
@Component({
  selector: 'app-display-dropdown',
  templateUrl: './display-dropdown.component.html',
  styleUrls: ['./display-dropdown.component.scss']
})


export class DisplayDropdownComponent implements OnInit {
  @Input() fieldData!:FieldType;
  optionList:OptionData[] = [];
  selectedOption!: OptionType;
  responseData: ResponseType = {
    fieldid: "",
    question: '',
    type: '',
    response: '',
  };
  responseChanges$ = new Subject<ResponseType>();

  constructor(private store: Store<{ response: ResponseState }>) { 
  }

  ngOnInit(): void {
    this.responseData = {
      fieldid: this.fieldData.fieldid,
      question: this.fieldData.question,
      type: this.fieldData.type,
      response: '',
    };
    if (this.fieldData.options) {
      this.optionList = this.fieldData.options.filter((option:string) => option.trim() !== "").map((option:string) => ({
        value: option.replace(/\s+/g, "").toLowerCase(),
        label: option,
      }));
    }
    this.responseChanges$.pipe(
      debounceTime(750), 
      distinctUntilChanged() 
    ).subscribe(() => {
      this.saveResponse();
    });
  }
  
  responseHandler(): void {
    this.responseChanges$.next({
      fieldid:"",
      type:"",
      question:"",
      response:['']
    }); 
  }
  
  saveResponse(): void {
    // Dispatch the response update action
    const updatedResponseData = {
      ...this.responseData,
      response: this.selectedOption?.label
    };
    this.store.dispatch(responseUpdate({ responseData: updatedResponseData }));
  }
  

}

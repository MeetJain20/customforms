import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayMultipleChoiceComponent } from './display-multiple-choice.component';

describe('DisplayMultipleChoiceComponent', () => {
  let component: DisplayMultipleChoiceComponent;
  let fixture: ComponentFixture<DisplayMultipleChoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayMultipleChoiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayMultipleChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

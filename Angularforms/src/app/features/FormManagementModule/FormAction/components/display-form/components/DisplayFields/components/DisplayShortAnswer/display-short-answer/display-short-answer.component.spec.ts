import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayShortAnswerComponent } from './display-short-answer.component';

describe('DisplayShortAnswerComponent', () => {
  let component: DisplayShortAnswerComponent;
  let fixture: ComponentFixture<DisplayShortAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayShortAnswerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayShortAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

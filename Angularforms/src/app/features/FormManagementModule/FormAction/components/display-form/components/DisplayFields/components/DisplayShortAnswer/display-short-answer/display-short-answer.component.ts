import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { debounceTime,Subject,distinctUntilChanged } from 'rxjs';
import { responseUpdate } from 'src/app/features/FormManagementModule/FormAction/store/actions/response.action';
import { ResponseState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/response.reducer';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';
import { ResponseType } from '../../../../../types/response.type';

@Component({
  selector: 'app-display-short-answer',
  templateUrl: './display-short-answer.component.html',
  styleUrls: ['./display-short-answer.component.scss']
})
export class DisplayShortAnswerComponent implements OnInit {
  @Input() fieldData!:FieldType;
  isAdmin:boolean = localStorage.getItem("role") === "admin";
  responseData: ResponseType = {
    fieldid: "",
    question: '',
    type: '',
    response: ''
  };
  responseChanges$ = new Subject<ResponseType>();

  constructor(private store:Store<{response: ResponseState}>) { }

  ngOnInit(): void {
    this.responseData = {
      fieldid: this.fieldData.fieldid,
      question: this.fieldData.question,
      type: this.fieldData.type,
      response: "",
    }
    this.responseChanges$.pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe(() => {
      this.saveResponse();
    });
  }

  responseHandler(): void {
    this.responseChanges$.next({
      fieldid:"",
      type:"",
      question:"",
      response:['']
    });
  }
  
  saveResponse(): void {
    const updatedResponseData = {
      ...this.responseData,
      response: this.responseData.response
    };
    this.store.dispatch(responseUpdate({ responseData: updatedResponseData }));
  }


}

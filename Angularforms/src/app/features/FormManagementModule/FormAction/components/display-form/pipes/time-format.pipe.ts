import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: string|string[]): string {
    if (!value) return value;

    let hours=0,minutes=0;
    if(typeof value === 'string')
    {
      [hours, minutes] = value.split(':').map(Number);
    }
    const period = hours >= 12 ? 'pm' : 'am';
    const twelveHourFormatHours = hours % 12 || 12;
    return `${twelveHourFormatHours}:${minutes < 10 ? '0' : ''}${minutes} ${period}`;
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-display-fields',
  templateUrl: './display-fields.component.html',
  styleUrls: ['./display-fields.component.scss']
})
export class DisplayFieldsComponent implements OnInit {

  @Input() fieldData!:FieldType;
  @Input() type!:string;

  constructor() { }

  ngOnInit(): void {
  }

}

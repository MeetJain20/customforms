import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayDropdownComponent } from './display-dropdown.component';

describe('DisplayDropdownComponent', () => {
  let component: DisplayDropdownComponent;
  let fixture: ComponentFixture<DisplayDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayDropdownComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

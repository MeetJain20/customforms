import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayCheckboxComponent } from './display-checkbox.component';

describe('DisplayCheckboxComponent', () => {
  let component: DisplayCheckboxComponent;
  let fixture: ComponentFixture<DisplayCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayCheckboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

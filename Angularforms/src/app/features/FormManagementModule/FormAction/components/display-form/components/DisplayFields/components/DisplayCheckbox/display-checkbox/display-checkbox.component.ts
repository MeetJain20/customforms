import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject, debounceTime, distinctUntilChanged } from 'rxjs';
import { responseUpdate } from 'src/app/features/FormManagementModule/FormAction/store/actions/response.action';
import { ResponseState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/response.reducer';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';
import { ResponseType } from '../../../../../types/response.type';

@Component({
  selector: 'app-display-checkbox',
  templateUrl: './display-checkbox.component.html',
  styleUrls: ['./display-checkbox.component.scss']
})
export class DisplayCheckboxComponent implements OnInit {
  @Input() fieldData!: FieldType;
  isAdmin: boolean = localStorage.getItem('role') === 'admin';
  responseData: any = {
    fieldid: "",
    question: '',
    type: '',
    response: [''],
  };
  responseChanges$ = new Subject<ResponseType>();

  constructor(private store: Store<{ response: ResponseState }>) {}

  ngOnInit(): void {
    this.responseData = {
      fieldid: this.fieldData.fieldid,
      question: this.fieldData.question,
      type: this.fieldData.type,
      response: [],
    };
    this.responseChanges$.pipe(
      debounceTime(750),
      distinctUntilChanged()
    ).subscribe(() => {
      this.saveResponse();
    });
  }

  responseHandler(option: string): void {
    const index = this.responseData.response.indexOf(option);
    if (index !== -1) {
      this.responseData.response.splice(index, 1);
    } else {
      this.responseData.response.push(option);
    }
    this.responseChanges$.next({
      fieldid:"",
      type:"",
      question:"",
      response:['']
    });
  }

  saveResponse(): void {
    // Dispatch the response update action
    const updatedResponseData = {
      ...this.responseData,
      response: [...this.responseData.response] // Copy array to prevent mutation
    };
    this.store.dispatch(responseUpdate({ responseData: updatedResponseData }));
  }
}

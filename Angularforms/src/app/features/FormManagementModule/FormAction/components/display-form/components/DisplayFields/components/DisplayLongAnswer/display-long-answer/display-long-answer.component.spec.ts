import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayLongAnswerComponent } from './display-long-answer.component';

describe('DisplayLongAnswerComponent', () => {
  let component: DisplayLongAnswerComponent;
  let fixture: ComponentFixture<DisplayLongAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayLongAnswerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayLongAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

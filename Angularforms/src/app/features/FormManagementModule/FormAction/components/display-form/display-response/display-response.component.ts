import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResponseType } from '../types/response.type';
import { ResponseData } from '../../../type/responsedata';

@Component({
  selector: 'app-display-response',
  templateUrl: './display-response.component.html',
  styleUrls: ['./display-response.component.scss']
})
export class DisplayResponseComponent implements OnInit {
totalResponseData!:ResponseData[];
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
       this.totalResponseData = JSON.parse(params['totalResponseData']);
       console.log(this.totalResponseData);
    });
  }

}

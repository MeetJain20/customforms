import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { responseUpdate } from 'src/app/features/FormManagementModule/FormAction/store/actions/response.action';
import { uploadFile } from 'src/app/features/FormManagementModule/FormAction/store/actions/uploadfiletos3.action';
import { FileState } from 'src/app/features/FormManagementModule/FormAction/store/reducers/uploadfiletos3.reducer';
import { uploadFileToResponse } from 'src/app/features/FormManagementModule/FormAction/store/selectors/uploadfiletos.selector';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';
import { ResponseType } from '../../../../../types/response.type';

@Component({
  selector: 'app-display-file',
  templateUrl: './display-file.component.html',
  styleUrls: ['./display-file.component.scss'],
})
export class DisplayFileComponent implements OnInit {
  @ViewChild('fileRef', { static: false }) fileInput: ElementRef | undefined;
  @Input() fieldData!: FieldType;
  isAdmin: boolean = localStorage.getItem('role') === 'admin';
  responseData: ResponseType = {
    fieldid: "",
    question: '',
    type: '',
    response: ''
  };
  constructor(private store:Store<{upload: FileState}>) {}

  ngOnInit(): void {
    this.responseData = {
      fieldid: this.fieldData.fieldid,
      question: this.fieldData.question,
      type: this.fieldData.type,
      response: "",
    }
  }

  responseHandler(event: any) {
    if (this.fileInput && this.fileInput.nativeElement.files[0]) {
     
        const file = this.fileInput.nativeElement.files[0];
        const formData = new FormData();
        formData.append('file', file);
        this.store.dispatch(uploadFile({newFormData:formData}));
        this.store.pipe(select(uploadFileToResponse)).subscribe((link)=>{
          const updatedResponseData = {
            ...this.responseData,
            response: link
          };
          this.store.dispatch(responseUpdate({responseData:updatedResponseData}));
        })
    }
  }
}

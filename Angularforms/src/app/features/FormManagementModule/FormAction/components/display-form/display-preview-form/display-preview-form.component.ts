import { Component, OnInit } from '@angular/core';
import { updateFormTitle } from '../../../store/actions/updateformtitle.action';
import { updateFormDesc } from '../../../store/actions/updateformdesc.action';
import { Store, select } from '@ngrx/store';
import { getCurrentFormDesc,getCurrentFormFields, getCurrentFormTitle } from '../../../store/selectors/getformdetails.selector';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormDetailsState } from '../../../store/reducers/getformdetails.reducer';
import { getFormDetails } from '../../../store/actions/getformdetails.action';
import { getFormResponse } from '../../../store/actions/getallresponse.action';
import { FieldType } from 'src/app/features/FormManagementModule/types/forms';

@Component({
  selector: 'app-display-preview-form',
  templateUrl: './display-preview-form.component.html',
  styleUrls: ['./display-preview-form.component.scss']
})
export class DisplayPreviewFormComponent implements OnInit {
  formId!: string;
  fields: FieldType[] = [];
  currentFormData: FormGroup;
  private formTitleSubscription!: Subscription; 
  private formDescSubscription!: Subscription; 
  private formFieldsSubscription!: Subscription; 


  constructor(
    private route: ActivatedRoute,
    private store: Store<{ getform: FormDetailsState }>,
    private router: Router,
  ) {
    this.currentFormData = new FormGroup({
      formtitle: new FormControl(''),
      formdesc: new FormControl(''),
      fields: new FormControl([]),
    });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.formId = params.get('id') || '';
    });
    this.store.dispatch(getFormDetails({ formid: this.formId }));
    this.store.dispatch(getFormResponse({formid:this.formId}));
    this.formTitleSubscription = this.store.pipe(select(getCurrentFormTitle)).subscribe((formtitle) => {
      this.currentFormData.get('formtitle')?.setValue(formtitle);
    });
    this.formDescSubscription = this.store.pipe(select(getCurrentFormDesc)).subscribe((formdesc) => {
      this.currentFormData.get('formdesc')?.setValue(formdesc);
    });
    this.formFieldsSubscription = this.store.pipe(select(getCurrentFormFields)).subscribe((fields) => {
      this.currentFormData.get('fields')?.setValue(fields);
      this.fields = fields;
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from the form fields subscription when the component is destroyed
    if (this.formFieldsSubscription) {
      this.formFieldsSubscription.unsubscribe();
    }
    if (this.formDescSubscription) {
      this.formDescSubscription.unsubscribe();
    }
    if (this.formTitleSubscription) {
      this.formTitleSubscription.unsubscribe();
    }
  }

}

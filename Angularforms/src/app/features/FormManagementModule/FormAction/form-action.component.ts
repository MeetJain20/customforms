import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormDetailsState } from './store/reducers/getformdetails.reducer';
import { Subscription } from 'rxjs';
import { getLoading } from './store/selectors/getformdetails.selector';

@Component({
  selector: 'app-form-action',
  templateUrl: './form-action.component.html',
  styleUrls: ['./form-action.component.scss']
})
export class FormActionComponent implements OnInit {
  isLoading: boolean = false;
  loadingSubscription: Subscription | undefined;

  constructor(private store: Store<{getform: FormDetailsState}>) { }

  ngOnInit(): void {
    this.loadingSubscription = this.store.pipe(select(getLoading)).subscribe((loading: boolean) => {
      this.isLoading = loading;
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
  }

}

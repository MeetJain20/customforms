import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormActionRoutingModule } from './form-action-routing.module';
import { FormActionComponent } from './form-action.component';
import { MakeFormComponent } from './components/make-form/make-form.component';
import { DisplayFormComponent } from './components/display-form/display-form.component';
import { getFormDetailsReducer } from './store/reducers/getformdetails.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GetFormEffects } from './store/effects/getformdetails.effects';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewFieldsComponent } from './components/make-form/components/NewFields/new-fields/new-fields.component';
import { FirstNewFieldsComponent } from './components/make-form/components/FirstNewFields/first-new-fields/first-new-fields.component';
import { DropdownModule } from 'primeng/dropdown';
import { ShortAnswerFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/ShortAnswerFields/short-answer-fields/short-answer-fields.component';
import { FieldTypeAttributesComponent } from './components/make-form/components/FieldTypeAttributes/field-type-attributes/field-type-attributes.component';
import { LongAnswerFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/LongAnswerFields/long-answer-fields/long-answer-fields.component';
import { CheckboxTypeFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/CheckboxTypeFields/checkbox-type-fields/checkbox-type-fields.component';
import { DateTypeFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/DateTypeFields/date-type-fields/date-type-fields.component';
import { FileTypeFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/FileTypeFields/file-type-fields/file-type-fields.component';
import { TimeTypeFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/TimeTypeFields/time-type-fields/time-type-fields.component';
import { DropdownFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/DropdownFields/dropdown-fields/dropdown-fields.component';
import { MultipleChoiceFieldsComponent } from './components/make-form/components/FieldTypeAttributes/components/MultipleChoiceFields/multiple-choice-fields/multiple-choice-fields.component';
import { FunctionalitesComponent } from './components/make-form/components/FieldTypeAttributes/components/Functionalities/functionalites/functionalites.component';
import { SaveFieldEffects } from './store/effects/savefield.effects';
import { OutsideClickDirective } from './components/make-form/directive/outside-click.directive';
import { SaveEditFormEffects } from './store/effects/saveandeditform.effects';
import { SaveFormTemplateEffects } from './store/effects/saveformtemplate.effects';
import { DeleteFieldEffects } from './store/effects/deletefield.effects';
import { AddNewFieldEffects } from './store/effects/addnewfield.effects';
import { UpdateTitleDescEffects } from './store/effects/updateTitleDesc.effects';
import { UseTemplateEffects } from './store/effects/usetemplate.effects';
import { DisplayFieldsComponent } from './components/display-form/components/DisplayFields/display-fields/display-fields.component';
import { DisplayCheckboxComponent } from './components/display-form/components/DisplayFields/components/DisplayCheckbox/display-checkbox/display-checkbox.component';
import { DisplayDateComponent } from './components/display-form/components/DisplayFields/components/DisplayDate/display-date/display-date.component';
import { DisplayDropdownComponent } from './components/display-form/components/DisplayFields/components/DisplayDropdown/display-dropdown/display-dropdown.component';
import { DisplayFileComponent } from './components/display-form/components/DisplayFields/components/DisplayFile/display-file/display-file.component';
import { DisplayLongAnswerComponent } from './components/display-form/components/DisplayFields/components/DisplayLongAnswer/display-long-answer/display-long-answer.component';
import { DisplayShortAnswerComponent } from './components/display-form/components/DisplayFields/components/DisplayShortAnswer/display-short-answer/display-short-answer.component';
import { DisplayMultipleChoiceComponent } from './components/display-form/components/DisplayFields/components/DisplayMultipleChoice/display-multiple-choice/display-multiple-choice.component';
import { DisplayTimeComponent } from './components/display-form/components/DisplayFields/components/DisplayTime/display-time/display-time.component';
import { responseReducer } from './store/reducers/response.reducer';
import { uploadFileReducer } from './store/reducers/uploadfiletos3.reducer';
import { UploadFileEffects } from './store/effects/uploadfiletos3.effects';
import { SaveFormResponseEffects } from './store/effects/saveresponse.effects';
import { saveResponseReducer } from './store/reducers/saveresponse.reducer';
import { GetFormResponseEffects } from './store/effects/getallresponse.effects';
import { getAllResponseReducer } from './store/reducers/getallresponse.reducer';
import { DisplayResponseComponent } from './components/display-form/display-response/display-response.component';
import { TimeFormatPipe } from './components/display-form/pipes/time-format.pipe';
import { DisplayPreviewFormComponent } from './components/display-form/display-preview-form/display-preview-form.component';
// import { LoaderComponent } from 'src/app/shared/Loader/loader/loader.component';
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/shared/shared.module';
@NgModule({
  declarations: [
    FormActionComponent,
    MakeFormComponent,
    DisplayFormComponent,
    NewFieldsComponent,
    FirstNewFieldsComponent,
    ShortAnswerFieldsComponent,
    FieldTypeAttributesComponent,
    LongAnswerFieldsComponent,
    CheckboxTypeFieldsComponent,
    DateTypeFieldsComponent,
    FileTypeFieldsComponent,
    TimeTypeFieldsComponent,
    DropdownFieldsComponent,
    MultipleChoiceFieldsComponent,
    FunctionalitesComponent,
    OutsideClickDirective,
    DisplayFieldsComponent,
    DisplayCheckboxComponent,
    DisplayDateComponent,
    DisplayDropdownComponent,
    DisplayFileComponent,
    DisplayLongAnswerComponent,
    DisplayShortAnswerComponent,
    DisplayMultipleChoiceComponent,
    DisplayTimeComponent,
    DisplayResponseComponent,
    TimeFormatPipe,
    DisplayPreviewFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DropdownModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forFeature('getform', getFormDetailsReducer),
    StoreModule.forFeature('response', responseReducer),
    StoreModule.forFeature('upload', uploadFileReducer),
    StoreModule.forFeature('saveresponse', saveResponseReducer),
    StoreModule.forFeature('allresponse', getAllResponseReducer),
    EffectsModule.forFeature([
      GetFormEffects,
      SaveFieldEffects,
      SaveEditFormEffects,
      SaveFormTemplateEffects,
      DeleteFieldEffects,
      AddNewFieldEffects,
      UpdateTitleDescEffects,
      UseTemplateEffects,
      UploadFileEffects,
      SaveFormResponseEffects,
      GetFormResponseEffects
    ]),
    FormActionRoutingModule,
  ],
})
export class FormActionModule {}

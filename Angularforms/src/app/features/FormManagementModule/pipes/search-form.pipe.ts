import { Pipe, PipeTransform } from '@angular/core';
import { FormData } from 'src/app/features/FormManagementModule/types/forms';

@Pipe({
  name: 'searchForm'
})
export class SearchFormPipe implements PipeTransform {
  transform(forms: FormData[], searchValue: string): FormData[] {
    if (!searchValue || searchValue.trim() === '') {
      return forms;
    }
    const searchTerm = searchValue.trim().toLowerCase().replace(/\s/g, '');
    return forms.filter(form => 
      form.formtitle?.toLowerCase().replace(/\s/g, '').includes(searchTerm)
    );
  }
}
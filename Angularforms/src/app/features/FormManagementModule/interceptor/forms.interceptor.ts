import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { logoutSuccess } from 'src/app/store/actions/login.action';
import { Store } from '@ngrx/store';

@Injectable()
export class FormsInterceptor implements HttpInterceptor {
  constructor(private router: Router,private store:Store) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const checkToken = localStorage.getItem('token');


    if (req.url.endsWith('login')) {
      return next.handle(req);
    }
    if (req.url.endsWith('signup')) {
      return next.handle(req);
    }

    if(!checkToken){
      this.store.dispatch(logoutSuccess());
    }

    const cloneReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${checkToken}`),
    });
    return next.handle(cloneReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.router.navigateByUrl('/login');
        }
        return throwError(error);
      })
    );
  }
}

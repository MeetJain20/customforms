import { TestBed } from '@angular/core/testing';

import { FormsInterceptor } from './forms.interceptor';

describe('FormsInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      FormsInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: FormsInterceptor = TestBed.inject(FormsInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MAIN_URL } from 'src/app/constants/constant';
import { FormData } from '../types/forms';
import { CreateFormId } from '../types/createforms';
import { MessageService } from 'primeng/api';
@Injectable({
  providedIn: 'root',
})
export class FormsService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  fetchCurrentFormItems(userId: string | null): Observable<FormData[]> {
    return this.http
      .get<FormData[]>(`${MAIN_URL}/form/getactiveforms/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Fetching Active Forms!',
          });

          throw new Error(error.message);
        })
      );
  }

  fetchTemplateFormItems(): Observable<FormData[]> {
    return this.http
      .get<FormData[]>(`${MAIN_URL}/form/gettemplateforms`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Fetching Template Forms!',
          });

          throw new Error(error.message);
        })
      );
  }

  fetchMyFormItems(userId: string | null): Observable<FormData[]> {
    return this.http
      .get<FormData[]>(`${MAIN_URL}/form/getcompletedforms/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Fetching My Forms!',
          });

          throw new Error(error.message);
        })
      );
  }

  fetchAssignedFormItems(userId: string | null): Observable<FormData[]> {
    return this.http
      .get<FormData[]>(`${MAIN_URL}/empform/getassignedforms/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Fetching Assigned Forms!',
          });

          throw new Error(error.message);
        })
      );
  }

  fetchSubmittedFormItems(userId: string | null): Observable<FormData[]> {
    return this.http
      .get<FormData[]>(`${MAIN_URL}/empform/getsubmittedforms/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Fetching Submitted Forms!',
          });
          throw new Error(error.message);
        })
      );
  }

  deleteFormm(formId: string | null): Observable<FormData> {
    return this.http
      .delete<FormData>(`${MAIN_URL}/form/deleteform`, {
        headers: {
          'Content-Type': 'application/json',
        },
        body: { formid: formId },
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Form deleted successfully!',
          });
        }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Deleting Form!',
          });
          throw new Error(error.message);
        })
      );
  }

  createFormm(adminId: string | null): Observable<CreateFormId> {
    return this.http
      .post<CreateFormId>(`${MAIN_URL}/form/createforms`, { adminId })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Form created successfully!',
          });
        }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Creating New Form!',
          });
          throw new Error(error.message);
        })
      );
  }
}

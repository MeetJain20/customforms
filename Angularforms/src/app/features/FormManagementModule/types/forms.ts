export interface FormData{
    _id:string,
    adminId:string,
    formtitle: string,
    formdesc:string,
    fields: FieldType[],
    isComplete: boolean,
    isTemplate: boolean,
};

export interface FieldType {
    fieldid:string,
    type:string,
    question: string;
    placeholder?: string;
    options?:string[];
    isrequired: boolean;
}

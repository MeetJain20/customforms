import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewFormState } from '../../store/reducers/createform.reducer';
import { Store, select } from '@ngrx/store';
import { getNewFormId } from '../../store/selectors/create.selector';
import { Router } from '@angular/router';
import { createForm } from '../../store/actions/createform.action';
import { Subscription } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss'],
})
export class AdminDashboardComponent implements OnInit, OnDestroy {
  private newFormIdSubscription: Subscription | undefined;

  constructor(
    private store: Store<{ create: NewFormState }>,
    private router: Router,
    private messageService:MessageService
  ) {}

  ngOnInit(): void {
  }
  
  createNewForm() {
    const userId = localStorage.getItem('userid');
    if (localStorage.getItem('role') === 'admin') {
      this.store.dispatch(createForm({ adminId: userId }));
      this.newFormIdSubscription = this.store
      .pipe(select(getNewFormId))
      .subscribe((id) => {
        if (id !== null) {

          this.router.navigateByUrl(`actions/createform/${id}`);
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.newFormIdSubscription) {
      this.newFormIdSubscription.unsubscribe();
    }
  }
}

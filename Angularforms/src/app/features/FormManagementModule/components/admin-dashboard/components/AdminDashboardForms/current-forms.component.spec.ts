import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentFormsComponent } from './current-forms.component';

describe('CurrentFormsComponent', () => {
  let component: CurrentFormsComponent;
  let fixture: ComponentFixture<CurrentFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentFormsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CurrentFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fetchCurrentFormItems } from 'src/app/features/FormManagementModule/store/actions/currentforms.action';
import { FormsState } from 'src/app/features/FormManagementModule/store/reducers/form.reducer';
import { getSearchText, selectCurrentForms, selectMyForms, selectTemplateForms } from 'src/app/features/FormManagementModule/store/selectors/forms.selector';
import { FormData } from 'src/app/features/FormManagementModule/types/forms';
import { fetchTemplateFormItems } from 'src/app/features/FormManagementModule/store/actions/templateforms.action';
import { fetchMyFormItems } from 'src/app/features/FormManagementModule/store/actions/myforms.action';

@Component({
  selector: 'app-current-forms',
  templateUrl: './current-forms.component.html',
  styleUrls: ['./current-forms.component.scss'],
})
export class CurrentFormsComponent implements OnInit, OnDestroy {
  forms: FormData[] = [];
  filteredForms: FormData[] = [];
  formtitle: string = 'Active Forms';
  searchString:string="";
  forms1: FormData[] = [];
  filteredForms1: FormData[] = [];
  formtitle1: string = 'Template Forms';
  forms2: FormData[] = [];
  filteredForms2: FormData[] = [];
  formtitle2: string = 'My Forms';
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store<{ form: FormsState }>) {}

  ngOnInit(): void {
    this.store.dispatch(fetchCurrentFormItems());
    this.store.dispatch(fetchTemplateFormItems());
    this.store.dispatch(fetchMyFormItems());

    this.store
      .pipe(select(selectCurrentForms), takeUntil(this.unsubscribe$))
      .subscribe((forms) => {
        this.forms = forms;
        this.filteredForms = forms;
      });

    this.store
      .pipe(select(selectTemplateForms), takeUntil(this.unsubscribe$))
      .subscribe((forms) => {
        this.forms1 = forms;
        this.filteredForms1 = forms;
      });

    this.store
      .pipe(select(selectMyForms), takeUntil(this.unsubscribe$))
      .subscribe((forms) => {
        this.forms2 = forms;
        this.filteredForms2 = forms;
      });

    this.store
      .pipe(select(getSearchText), takeUntil(this.unsubscribe$))
      .subscribe((search) => {
        this.searchString = search;
        this.updateForms();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private updateForms(): void {
    if (!this.searchString || this.searchString.trim() === '') {
      this.filteredForms = this.forms;
      this.filteredForms1 = this.forms1;
      this.filteredForms2 = this.forms2;
    } else {
      const searchTerm = this.searchString.trim().toLowerCase().replace(/\s/g, '');
      this.filteredForms = this.forms.filter(form =>
        form.formtitle?.toLowerCase().replace(/\s/g, '').includes(searchTerm)
      );
      this.filteredForms1 = this.forms1.filter(form =>
        form.formtitle?.toLowerCase().replace(/\s/g, '').includes(searchTerm)
      );
      this.filteredForms2 = this.forms2.filter(form =>
        form.formtitle?.toLowerCase().replace(/\s/g, '').includes(searchTerm)
      );
    }
  }
}

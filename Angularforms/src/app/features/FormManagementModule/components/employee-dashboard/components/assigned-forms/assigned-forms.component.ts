import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormData } from 'src/app/features/FormManagementModule/types/forms';
import { FormsState } from 'src/app/features/FormManagementModule/store/reducers/form.reducer';
import { fetchAssignedFormItems } from 'src/app/features/FormManagementModule/store/actions/assignedforms.action';
import { fetchSubmittedFormItems } from 'src/app/features/FormManagementModule/store/actions/submittedforms.action';
import { getSearchText, selectAssignedForms, selectSubmittedForms } from 'src/app/features/FormManagementModule/store/selectors/forms.selector';

@Component({
  selector: 'app-assigned-forms',
  templateUrl: './assigned-forms.component.html',
  styleUrls: ['./assigned-forms.component.scss']
})
export class AssignedFormsComponent implements OnInit, OnDestroy {

  formContainers: {
    forms: FormData[];
    filteredForms: FormData[];
    formTitle: string;
    subscription: Subscription | undefined;
  }[] = [];

  searchString: string = "";
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private store: Store<{ form: FormsState }>) {}

  ngOnInit(): void {
    this.store.dispatch(fetchAssignedFormItems());
    this.store.dispatch(fetchSubmittedFormItems());

    this.initializeForms([
      { selector: selectAssignedForms, title: 'Assigned Forms' },
      { selector: selectSubmittedForms, title: 'Submitted Forms' },
    ]);

    this.store.pipe(select(getSearchText), takeUntil(this.unsubscribe$)).subscribe(search => {
      this.searchString = search;
      this.updateForms();
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initializeForms(formConfigs: { selector: any; title: string }[]): void {
    formConfigs.forEach(config => {
      const subscription = this.store.pipe(select(config.selector), takeUntil(this.unsubscribe$)).subscribe(forms => {
        const filteredForms = this.filterForms(forms);
        this.formContainers.push({
          forms: forms,
          filteredForms: filteredForms,
          formTitle: config.title,
          subscription: subscription
        });
      });
    });
  }

  filterForms(forms: FormData[]): FormData[] {
    if (!this.searchString || this.searchString.trim() === '') {
      return forms;
    } else {
      const searchTerm = this.searchString.trim().toLowerCase().replace(/\s/g, '');
      return forms.filter(form =>
        form.formtitle?.toLowerCase().replace(/\s/g, '').includes(searchTerm)
      );
    }
  }

  updateForms(): void {
    this.formContainers.forEach(container => {
      container.filteredForms = this.filterForms(container.forms);
    });
  }
}

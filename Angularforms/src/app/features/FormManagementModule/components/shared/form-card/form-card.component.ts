import { Component, Input, OnInit } from '@angular/core';
import { FormData } from '../../../types/forms';
import { Store } from '@ngrx/store';
import { deleteForm } from '../../../store/actions/deleteform.action';
import { FormsState } from '../../../store/reducers/form.reducer';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-card',
  templateUrl: './form-card.component.html',
  styleUrls: ['./form-card.component.scss'],
})
export class FormCardComponent {
  isSubmittedForm: boolean = false;
  @Input() formtitle: string | null = '';
  @Input() form!: FormData;
  constructor(private store: Store<{ form: FormsState }>, private router: Router) {
    // console.log(this.form);
  }
  // ngOnInit(): void {
  // }

  openForm(){
    if(this.formtitle === 'Active Forms')
    {
      this.router.navigateByUrl(`actions/createform/${this.form._id}`);
    }
    else{
      this.router.navigateByUrl(`actions/displayform/${this.form._id}`)
    }
  }

  deleteSelectedform(event:MouseEvent) {
    event.stopPropagation()
    this.store.dispatch(deleteForm({formId: this.form._id}));
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormData } from 'src/app/features/FormManagementModule/types/forms';
import { FormsState } from '../../../store/reducers/form.reducer';
import { searchValue } from '../../../store/actions/searchForm.action';

@Component({
  selector: 'app-form-list-container',
  templateUrl: './form-list-container.component.html',
  styleUrls: ['./form-list-container.component.scss']
})
export class FormListContainerComponent implements OnInit {
  @Input() formtitle: string | null = "";
  @Input() forms: FormData[] = [];
  searchText: string = '';
  filteredForms: FormData[] = [];
  showAllForms: boolean = false;

  constructor(private store: Store<{ form: FormsState }>) { }

  ngOnInit(): void {
    this.filteredForms = [...this.forms]; // Initially, display all forms
  }

  toggleShowAllForms() {
    this.showAllForms = !this.showAllForms;
  }
  searchAction(){
    this.store.dispatch(searchValue({searchString:this.searchText}));
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormListContainerComponent } from './form-list-container.component';

describe('FormListContainerComponent', () => {
  let component: FormListContainerComponent;
  let fixture: ComponentFixture<FormListContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormListContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { EmployeeDashboardComponent } from './components/employee-dashboard/employee-dashboard.component';
import { CurrentFormsComponent } from './components/admin-dashboard/components/AdminDashboardForms/current-forms.component';
import { FormListContainerComponent } from './components/shared/form-list-container/form-list-container.component';
import { FormCardComponent } from './components/shared/form-card/form-card.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FormsEffects } from './store/effects/currentforms.effects';
import { formsReducer } from './store/reducers/form.reducer';
import { MyFormsEffects } from './store/effects/myforms.effects';
import { TemplateFormsEffects } from './store/effects/templateforms.effects';
import { AssignedFormsComponent } from './components/employee-dashboard/components/assigned-forms/assigned-forms.component';
import { AssignedFormsEffects } from './store/effects/assignedforms.effects';
import { SubmittedFormsEffects } from './store/effects/submittedforms.effects';
import { FormsModule } from '@angular/forms';
import { SearchFormPipe } from './pipes/search-form.pipe';
import { DeleteEffects } from './store/effects/deleteform.effects';
import { HttpClientModule } from '@angular/common/http';
import { CreateFormEffects } from './store/effects/createform.effects';
import { createFormReducer } from './store/reducers/createform.reducer';
// import { LoaderComponent } from 'src/app/shared/Loader/loader/loader.component';
import { AppModule } from 'src/app/app.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    FormsComponent,
    AdminDashboardComponent,
    EmployeeDashboardComponent,
    CurrentFormsComponent,
    FormCardComponent,
    FormListContainerComponent,
    AssignedFormsComponent,
    SearchFormPipe,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HttpClientModule,
    FormsRoutingModule,
    StoreModule.forFeature('forms', formsReducer),
    StoreModule.forFeature('create', createFormReducer),
    EffectsModule.forFeature([
      FormsEffects,
      MyFormsEffects,
      TemplateFormsEffects,
      AssignedFormsEffects,
      SubmittedFormsEffects,
      DeleteEffects,
      CreateFormEffects
    ]),
  ],
 
})
export class FormManagementModule {}

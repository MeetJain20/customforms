import { createReducer, on } from '@ngrx/store';
import {
  fetchCurrentFormItems,
  fetchCurrentFormItemsSuccess,
  fetchCurrentFormItemsFailure,
} from '../actions/currentforms.action';
import {
  fetchTemplateFormItems,
  fetchTemplateFormItemsSuccess,
  fetchTemplateFormItemsFailure,
} from '../actions/templateforms.action';
import {
  fetchMyFormItems,
  fetchMyFormItemsSuccess,
  fetchMyFormItemsFailure,
} from '../actions/myforms.action';
import {
  fetchAssignedFormItems,
  fetchAssignedFormItemsSuccess,
  fetchAssignedFormItemsFailure,
} from '../actions/assignedforms.action';
import {
  fetchSubmittedFormItems,
  fetchSubmittedFormItemsSuccess,
  fetchSubmittedFormItemsFailure,
} from '../actions/submittedforms.action';
import { searchValue } from '../actions/searchForm.action';
import { FormData } from '../../types/forms';
import { deleteForm,deleteFormSuccess,deleteFormFailure } from '../actions/deleteform.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';


export interface FormsState {
  currentForms: FormData[];
  templateForms: FormData[];
  myForms: FormData[];
  assignedForms: FormData[];
  submittedForms: FormData[];
  searchString: string;
  loading: boolean;
  error: string | null;
}

export const initialState: FormsState = {
  currentForms: [],
  templateForms: [],
  myForms: [],
  assignedForms: [],
  submittedForms: [],
  searchString:"",
  loading: false,
  error: null,
};

export const formsReducer = createReducer(
  initialState,
  // Current Forms
  on(fetchCurrentFormItems, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(fetchCurrentFormItemsSuccess, (state, { forms }) => ({
    ...state,
    currentForms: forms,
    loading: false,
  })),
  on(fetchCurrentFormItemsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  // Template Forms
  on(fetchTemplateFormItems, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(fetchTemplateFormItemsSuccess, (state, { forms }) => ({
    ...state,
    templateForms: forms,
    loading: false,
  })),
  on(fetchTemplateFormItemsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  // My Forms
  on(fetchMyFormItems, (state) => ({ ...state, loading: true, error: null })),
  on(fetchMyFormItemsSuccess, (state, { forms }) => ({
    ...state,
    myForms: forms,
    loading: false,
  })),
  on(fetchMyFormItemsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  // Assigned Forms
  on(fetchAssignedFormItems, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(fetchAssignedFormItemsSuccess, (state, { forms }) => ({
    ...state,
    assignedForms: forms,
    loading: false,
  })),
  on(fetchAssignedFormItemsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  // Submitted Forms
  on(fetchSubmittedFormItems, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(fetchSubmittedFormItemsSuccess, (state, { forms }) => ({
    ...state,
    submittedForms: forms,
    loading: false,
  })),
  on(fetchSubmittedFormItemsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(searchValue, (state,{ searchString }) => ({
    ...state,
    searchString
  })), 

  on(deleteForm, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(deleteFormSuccess, (state, { formId }) => ({
    ...state,
    loading: false,
    currentForms: state.currentForms.filter(form => form._id !== formId),
    myForms: state.myForms.filter(form => form._id !== formId),
    templateForms: state.templateForms.filter(form => form._id !== formId),
    assignedForms: state.assignedForms.filter(form => form._id !== formId),
    submittedForms: state.submittedForms.filter(form => form._id !== formId),
  })),
  on(deleteFormFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(logoutSuccess, ()=>initialState)

);

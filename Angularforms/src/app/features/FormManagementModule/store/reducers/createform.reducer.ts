// form.reducer.ts
import { createReducer, on } from '@ngrx/store';
import { createForm,createFormFailure,createFormSuccess } from '../actions/createform.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';

export interface NewFormState {
  loading: boolean;
  error: string;
  createdFormId: string | null;
}

export const initialState: NewFormState = {
  loading: false,
  error: "",
  createdFormId: null,
};

export const createFormReducer = createReducer(
  initialState,
  on(createForm, (state) => ({
    ...state,
    loading: true,
    error: "",
    createdFormId: null,
  })),
  on(createFormSuccess, (state, { id }) => ({
    ...state,
    loading: false,
    createdFormId: id,
  })),
  on(createFormFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error: error,
    createdFormId: null,
  })),
  on(logoutSuccess, ()=>initialState)

);

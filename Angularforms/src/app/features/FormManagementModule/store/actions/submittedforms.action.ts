import { createAction, props } from '@ngrx/store';
import { FormData } from '../../types/forms';
export const fetchSubmittedFormItems = createAction('[Submitted Forms] Fetch Submitted Forms');
export const fetchSubmittedFormItemsSuccess = createAction(
  '[Submitted Forms] Fetch Submitted Forms Success',
  props<{ forms: FormData[] }>()
);
export const fetchSubmittedFormItemsFailure = createAction(
  '[Submitted Forms] Fetch Submitted Forms Failure',
  props<{ error: string }>()
);
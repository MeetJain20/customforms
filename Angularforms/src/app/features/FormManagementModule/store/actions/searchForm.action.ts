import { createAction, props } from '@ngrx/store';

export const searchValue = createAction('[Search Value] Search String',
props<{ searchString: string }>());
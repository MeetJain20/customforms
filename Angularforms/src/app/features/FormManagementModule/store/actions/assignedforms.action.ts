
import { createAction, props } from '@ngrx/store';
import { FormData } from '../../types/forms';
export const fetchAssignedFormItems = createAction('[Assigned Forms] Fetch Assigned Forms');
export const fetchAssignedFormItemsSuccess = createAction(
  '[Assigned Forms] Fetch Assigned Forms Success',
  props<{ forms: FormData[] }>()
);
export const fetchAssignedFormItemsFailure = createAction(
  '[Assigned Forms] Fetch Assigned Forms Failure',
  props<{ error: string }>()
);
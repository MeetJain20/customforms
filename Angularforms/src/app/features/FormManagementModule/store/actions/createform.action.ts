import { createAction, props } from '@ngrx/store';

export const createForm = createAction(
  '[Create Form] Create Form',
  props<{ adminId: string|null }>()
);

export const createFormSuccess = createAction(
  '[Create Form] Create Form Success',
  props<{ id: string }>()
);

export const createFormFailure = createAction(
  '[Create Form] Create Form Failure',
  props<{ error: string }>()
);
import { createAction, props } from '@ngrx/store';

export const deleteForm = createAction('[Delete Form] Delete Form', props<{ formId: string|null }>());
export const deleteFormSuccess = createAction('[Delete Form] Delete Form Success',props<{ formId: string|null }>());
export const deleteFormFailure = createAction('[Delete Form] Delete Form Failure',  props<{ error: string }>());

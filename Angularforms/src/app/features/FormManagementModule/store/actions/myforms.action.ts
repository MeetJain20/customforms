import { createAction, props } from '@ngrx/store';
import { FormData } from '../../types/forms';

export const fetchMyFormItems = createAction('[My Forms] Fetch My Forms');

export const fetchMyFormItemsSuccess = createAction(
  '[My Forms] Fetch My Forms Success',
  props<{ forms: FormData[] }>()
);

export const fetchMyFormItemsFailure = createAction(
  '[My Forms] Fetch My Forms Failure',
  props<{ error: string }>()
);

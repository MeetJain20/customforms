import { createAction, props } from '@ngrx/store';
import { FormData } from '../../types/forms';
export const fetchCurrentFormItems = createAction('[Current Forms] Fetch Current Forms');
export const fetchCurrentFormItemsSuccess = createAction(
  '[Current Forms] Fetch Current Forms Success',
  props<{ forms: FormData[] }>()
);
export const fetchCurrentFormItemsFailure = createAction(
  '[Current Forms] Fetch Current Forms Failure',
  props<{ error: string }>()
);
import { createAction, props } from '@ngrx/store';
import { FormData } from '../../types/forms';

export const fetchTemplateFormItems = createAction('[Template Forms] Fetch Template Forms');

export const fetchTemplateFormItemsSuccess = createAction(
  '[Template Forms] Fetch Template Forms Success',
  props<{ forms: FormData[] }>()
);

export const fetchTemplateFormItemsFailure = createAction(
  '[Template Forms] Fetch Template Forms Failure',
  props<{ error: string }>()
);

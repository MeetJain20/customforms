import { createSelector, createFeatureSelector } from '@ngrx/store';
import { NewFormState } from '../reducers/createform.reducer';
export const newFormState = createFeatureSelector<NewFormState>('create');

export const getNewFormId = createSelector(
    newFormState,
  (state: NewFormState) => state.createdFormId
);
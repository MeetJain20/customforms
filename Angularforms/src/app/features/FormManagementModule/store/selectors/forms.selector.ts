import { createSelector, createFeatureSelector } from '@ngrx/store';
import { FormsState } from '../reducers/form.reducer'; // Assuming this is the path to your FormsState interface

// Create a feature selector to select the forms state
export const selectFormsState = createFeatureSelector<FormsState>('forms');

// Define a selector function to select the forms array from the forms state
export const selectCurrentForms = createSelector(
  selectFormsState,
  (state: FormsState) => state.currentForms // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const selectTemplateForms = createSelector(
  selectFormsState,
  (state: FormsState) => state.templateForms // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const selectMyForms = createSelector(
  selectFormsState,
  (state: FormsState) => state.myForms // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const selectAssignedForms = createSelector(
  selectFormsState,
  (state: FormsState) => state.assignedForms // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const selectSubmittedForms = createSelector(
  selectFormsState,
  (state: FormsState) => state.submittedForms // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const getSearchText = createSelector(
  selectFormsState,
  (state: FormsState) => state.searchString // Assuming 'forms' is the property containing the forms array in your FormsState interface
);
export const getLoading = createSelector(
  selectFormsState,
  (state: FormsState) => state.loading // Assuming 'forms' is the property containing the forms array in your FormsState interface
);



// form.effects.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { createForm,createFormFailure,createFormSuccess } from '../actions/createform.action';

@Injectable()
export class CreateFormEffects {
  constructor(private actions$: Actions, private formsService: FormsService) {}

  createForm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createForm),
      switchMap((action) =>
        this.formsService.createFormm(action.adminId).pipe(
          map((response) => createFormSuccess({ id: response.id })),
          catchError((error) =>
            of(createFormFailure({ error }))
          )
        )
      )
    )
  );
}

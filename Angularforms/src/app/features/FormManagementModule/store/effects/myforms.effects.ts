import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  fetchMyFormItems,
  fetchMyFormItemsSuccess,
  fetchMyFormItemsFailure,
} from '../actions/myforms.action';

@Injectable()
export class MyFormsEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  fetchMyFormItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchMyFormItems),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .fetchMyFormItems(auth.userId)
          .pipe(
            map((forms) => fetchMyFormItemsSuccess({ forms })),
            catchError((error) =>
              of(fetchMyFormItemsFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  fetchSubmittedFormItems,
  fetchSubmittedFormItemsSuccess,
  fetchSubmittedFormItemsFailure,
} from '../actions/submittedforms.action';
@Injectable()
export class SubmittedFormsEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  fetchSubmittedFormItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchSubmittedFormItems),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .fetchSubmittedFormItems(auth.userId)
          .pipe(
            map((forms) => fetchSubmittedFormItemsSuccess({ forms })),
            catchError((error) =>
              of(fetchSubmittedFormItemsFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

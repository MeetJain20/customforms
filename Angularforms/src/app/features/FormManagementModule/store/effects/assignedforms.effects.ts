import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  fetchAssignedFormItems,
  fetchAssignedFormItemsSuccess,
  fetchAssignedFormItemsFailure,
} from '../actions/assignedforms.action';
@Injectable()
export class AssignedFormsEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  fetchAssignedFormItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchAssignedFormItems),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .fetchAssignedFormItems(auth.userId)
          .pipe(
            map((forms) => fetchAssignedFormItemsSuccess({ forms })),
            catchError((error) =>
              of(fetchAssignedFormItemsFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

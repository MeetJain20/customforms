import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  deleteForm,
  deleteFormFailure,
  deleteFormSuccess,
} from '../actions/deleteform.action';

@Injectable()
export class DeleteEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  deleteForms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteForm),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .deleteFormm(action.formId)
          .pipe(
            map((forms) => deleteFormSuccess({formId:action.formId})),
            catchError((error) =>
              of(deleteFormFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  fetchCurrentFormItems,
  fetchCurrentFormItemsSuccess,
  fetchCurrentFormItemsFailure,
} from '../actions/currentforms.action';

@Injectable()
export class FormsEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  fetchCurrentFormItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchCurrentFormItems),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .fetchCurrentFormItems(auth.userId)
          .pipe(
            map((forms) => fetchCurrentFormItemsSuccess({ forms })),
            catchError((error) =>
              of(fetchCurrentFormItemsFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

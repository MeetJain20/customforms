import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { FormsService } from '../../services/forms.service';
import { Store, select } from '@ngrx/store';
import { AuthState } from 'src/app/store/reducers/login.reducer';
import {
  fetchTemplateFormItems,
  fetchTemplateFormItemsSuccess,
  fetchTemplateFormItemsFailure,
} from '../actions/templateforms.action';

@Injectable()
export class TemplateFormsEffects {
  constructor(
    private actions$: Actions,
    private formsService: FormsService,
    private store: Store<{ auth: AuthState }>
  ) {}

  fetchTemplateFormItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchTemplateFormItems),
      withLatestFrom(this.store.pipe(select((state) => state.auth))),
      switchMap(([action, auth]) =>
        this.formsService
          .fetchTemplateFormItems()
          .pipe(
            map((forms) => fetchTemplateFormItemsSuccess({ forms })),
            catchError((error) =>
              of(fetchTemplateFormItemsFailure({ error: error.message }))
            )
          )
      )
    )
  );
}

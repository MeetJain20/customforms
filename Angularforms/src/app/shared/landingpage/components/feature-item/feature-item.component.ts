import { Component, Input, OnInit } from '@angular/core';
import { Feature } from '../../types/types';

@Component({
  selector: 'app-feature-item',
  templateUrl: './feature-item.component.html',
  styleUrls: ['./feature-item.component.scss']
})
export class FeatureItemComponent implements OnInit {

  title:string="";
  description:string="";
  @Input() feature:Feature={
    title:"",
    description:"",
    image:""
  }
  @Input() side:string|null="";
  constructor() { }

  ngOnInit(): void {
  }

}

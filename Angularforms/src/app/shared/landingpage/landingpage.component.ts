import { Component, OnInit } from '@angular/core';
import { Feature } from './types/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthState } from 'src/app/store/reducers/login.reducer';
@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {
  isLoggedIn$:Observable<boolean>;
  role$:Observable<string|null>;
  features:Feature[] = [
    {
      title: "Create an online form as easily as creating a document",
      description:
        "Select from multiple question types and customize values as easily as pasting a list.",
      image: "assets/formcreation.jpg",
    },
    {
      title: "Send polished surveys and forms",
      description:
        "Customize colors, images, and fonts to adjust the look and feel or reflect your organization’s branding. And add custom logic that shows questions based on answers, for a more seamless experience.",
      image: "assets/sendformforresponse.jpg",
    },
    {
      title: "Appove the response received",
      description:
        "Based on the response received from employee decide whether to approve it or not",
      image: "assets/approveresponse.jpg",
    },
  ];
  constructor(private store: Store<{ auth: AuthState }>, private router: Router) { 
    this.isLoggedIn$ = store.select((state) => state.auth.isLoggedIn);
    this.role$ = this.store.select((state) => state.auth.role);
    // this.isLoggedIn$.subscribe(isLoggedIn => {
    //   console.log(isLoggedIn);
    // });
  }
  ngOnInit(): void {
  }

  navigatetodashboard(){
    this.role$.subscribe(role => {
      if(role === 'admin')
      {
        this.router.navigate(['forms/admindashboard']);
      }else if(role === 'employee'){
        this.router.navigate(['forms/employeedashboard']);
      }
    });
  }

  gotoSignup(){
    this.router.navigateByUrl('/signup');
  }

}

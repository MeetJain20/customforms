import { createSelector, createFeatureSelector } from '@ngrx/store';

import { ProfileState } from './profile.reducer';
export const selectProfileState = createFeatureSelector<ProfileState>('profile');

export const getName = createSelector(
    selectProfileState,
  (state: ProfileState) => state.empName 
);
export const getMobile = createSelector(
    selectProfileState,
  (state: ProfileState) => state.mobile 
);
export const getEmail = createSelector(
    selectProfileState,
  (state: ProfileState) => state.email 
);
export const getTeamName = createSelector(
    selectProfileState,
  (state: ProfileState) => state.teamName 
);
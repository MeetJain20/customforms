import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { fetchAdminProfileData,fetchEmployeeProfileData,fetchProfileDataFailure,fetchProfileDataSuccess,updateAdminProfileData,updateEmployeeProfileData,updateProfileDataFailure,updateProfileDataSuccess } from './profile.action';
import { ProfileService } from '../services/profile.service';

@Injectable()
export class ProfileEffects {
  constructor(
    private actions$: Actions,
    private profileService: ProfileService
  ) {}

  fetchAdminProfileData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchAdminProfileData),
      switchMap(() =>
        this.profileService.fetchAdminProfileData().pipe(
          map(({ empName, mobile,teamName,email }) => fetchProfileDataSuccess({ empName, mobile,teamName,email})),
          catchError(error => of(fetchProfileDataFailure({ error })))
        )
      )
    )
  );

  fetchEmployeeProfileData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchEmployeeProfileData),
      switchMap(() =>
        this.profileService.fetchEmployeeProfileData().pipe(
            map(({ empName, mobile,teamName,email }) => fetchProfileDataSuccess({ empName, mobile,teamName,email})),
            catchError(error => of(fetchProfileDataFailure({ error })))
        )
      )
    )
  );

  updateAdminProfileData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateAdminProfileData),
      mergeMap((action) =>
        this.profileService.updateAdminProfile(action.id, action.empName, action.mobile, action.email).pipe(
          map(({empName,mobile,email}) => updateProfileDataSuccess({empName,mobile,email})),
          catchError((error) => of(updateProfileDataFailure({ error: error.message })))
        )
      )
    )
  );

  updateEmployeeProfileData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateEmployeeProfileData),
      mergeMap((action) =>
        this.profileService.updateEmployeeProfile(action.id, action.empName, action.mobile, action.email).pipe(
          map(({empName,mobile,email}) => updateProfileDataSuccess({empName,mobile,email})),
          catchError((error) => of(updateProfileDataFailure({ error: error.message })))
        )
      )
    )
  );

}

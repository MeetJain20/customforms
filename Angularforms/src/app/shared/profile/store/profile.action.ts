
import { createAction, props } from '@ngrx/store';

export const fetchAdminProfileData = createAction('[Profile] Fetch Admin Profile Data');
export const fetchEmployeeProfileData = createAction('[Profile] Fetch Employee Profile Data');
export const fetchProfileDataSuccess = createAction('[Profile] Fetch Profile Data Success', props<{ empName:string,teamName:string,mobile:number,email:string }>());
export const fetchProfileDataFailure = createAction('[Profile] Fetch Profile Data Failure', props<{ error: string }>());


export const updateAdminProfileData = createAction('[Profile] Update Admin Profile Data', props<{ id: string|null, empName: string, mobile: number, email: string }>());
export const updateEmployeeProfileData = createAction('[Profile] Update Employee Profile Data', props<{ id: string|null, empName: string, mobile: number, email: string }>());
export const updateProfileDataSuccess = createAction('[Profile] Update Profile Data Success', props<{ empName: string, mobile: number, email: string }>());
export const updateProfileDataFailure = createAction('[Profile] Update Profile Data Failure', props<{ error: string }>());
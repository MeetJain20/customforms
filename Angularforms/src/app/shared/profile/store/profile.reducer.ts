import { createReducer, on } from '@ngrx/store';
import {
  fetchAdminProfileData,
  fetchEmployeeProfileData,
  fetchProfileDataFailure,
  fetchProfileDataSuccess,
  updateAdminProfileData,
  updateEmployeeProfileData,
  updateProfileDataFailure,
  updateProfileDataSuccess,
} from './profile.action';
import { logoutSuccess } from 'src/app/store/actions/login.action';
// import { ProfileData } from './profile.types';

export interface ProfileState {
  empName: string;
  email: string;
  mobile: number;
  teamName: string;
  loading: boolean;
  error: string;
}

export const initialState: ProfileState = {
  empName: '',
  email: '',
  mobile: 0,
  teamName: '',
  loading: false,
  error: '',
};

export const profileReducer = createReducer(
  initialState,
  on(fetchAdminProfileData, fetchEmployeeProfileData, (state) => ({
    ...state,
    loading: true,
  })),
  on(
    fetchProfileDataSuccess,
    (state, { empName, mobile, email, teamName }) => ({
      ...state,
      empName,
      mobile,
      teamName,
      email,
      loading: false,
      error:"",
    })
  ),
  on(fetchProfileDataFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(updateAdminProfileData, updateEmployeeProfileData, (state) => ({
    ...state,
    loading: true,
    error: "",
  })),
  on(updateProfileDataSuccess, (state, { email, empName, mobile }) => ({
    ...state,
    email,
    empName,
    mobile,
    loading: false,
  })),
  on(updateProfileDataFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),

  on(logoutSuccess, ()=>initialState)

);

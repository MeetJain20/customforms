import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ProfileState } from '../store/profile.reducer';
import { Store, select } from '@ngrx/store';
import {
  fetchAdminProfileData,
  fetchEmployeeProfileData,
  updateAdminProfileData,
  updateEmployeeProfileData,
} from '../store/profile.action';
import {
  getEmail,
  getMobile,
  getName,
  getTeamName,
} from '../store/profile.selector';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  @Output() closemodal = new EventEmitter();
  profileFormData: FormGroup;
  isChange:boolean=false;
  unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store<{ profile: ProfileState }>,
    private router: Router,
    private messageService:MessageService
  ) {
    this.profileFormData = new FormGroup({
      empName: new FormControl('', [
        Validators.required,
        this.noEmptySpacesValidator,
      ]),
      email: new FormControl('', [
        Validators.email,
        this.noEmptySpacesValidator,
      ]),
      mobile: new FormControl(0, [Validators.required]),
      teamName: new FormControl(''),
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('role') === 'admin') {
      this.store.dispatch(fetchAdminProfileData());
    } else if (localStorage.getItem('role') === 'employee') {
      this.store.dispatch(fetchEmployeeProfileData());
    }

   this.store.pipe(select(getEmail), takeUntil(this.unsubscribe$)).subscribe((email) => {
      this.profileFormData.get('email')?.setValue(email);
    });
    this.store.pipe(select(getMobile), takeUntil(this.unsubscribe$)).subscribe((mobile) => {
      this.profileFormData.get('mobile')?.setValue(mobile);
    });
   this.store.pipe(select(getTeamName), takeUntil(this.unsubscribe$)).subscribe((teamName) => {
      this.profileFormData.get('teamName')?.setValue(teamName);
    });
   this.store.pipe(select(getName), takeUntil(this.unsubscribe$)).subscribe((empName) => {
      this.profileFormData.get('empName')?.setValue(empName);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  editProfile() {
    const emailControl = this.profileFormData.get('email');
    if (emailControl?.invalid) {
      if (emailControl.errors?.['email']) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning',
          detail: 'Please enter a valid email address!',
        });
      }
      return;
    }

    if (this.profileFormData.invalid) {
      this.messageService.add({severity:'warn', summary:'Warning', detail:'Please fill in all fields!'});
      return;
    }
    
    const id = localStorage.getItem('userid');
    if (localStorage.getItem('role') === 'admin') {
      this.store.dispatch(
        updateAdminProfileData({
          id: id,
          empName: this.profileFormData.get('empName')?.value,
          mobile: this.profileFormData.get('mobile')?.value,
          email: this.profileFormData.get('email')?.value,
        })
      );
    } else if (localStorage.getItem('role') === 'employee') {
      this.store.dispatch(
        updateEmployeeProfileData({
          id: id,
          empName: this.profileFormData.get('empName')?.value,
          mobile: this.profileFormData.get('mobile')?.value,
          email: this.profileFormData.get('email')?.value,
        })
      );
    }
    this.closemodal.emit();
  }

  changeHandler(){
    this.isChange = true;
  }

  closeModalFromButton() {
    this.closemodal.emit();
  }

  noEmptySpacesValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    if (control.value && control.value.trim().length === 0) {
      return { emptySpaces: true };
    }
    return null;
  }
}

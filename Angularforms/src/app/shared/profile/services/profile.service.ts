import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, map, tap } from 'rxjs';
import { MAIN_URL } from 'src/app/constants/constant';
import { ProfileData } from '../types/profile.types';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private http: HttpClient,private messageService:MessageService) {}

  fetchAdminProfileData(): Observable<ProfileData> {
    const userId = localStorage.getItem('userid');
    return this.http
      .get<ProfileData>(`${MAIN_URL}/profile/getadmprofiledetails/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
      
        map((response: any) => {
          return {
            empName: response[0].empName,
            mobile: response[0].mobile,
            email: response[0].email,
            teamName: response[0].teamName,
          } as ProfileData;
        }),
        catchError((error) => {
          
          throw new Error(error.message);
        })
      );
  }

  fetchEmployeeProfileData(): Observable<ProfileData> {
    const userId = localStorage.getItem('userid');
    return this.http
      .get<ProfileData>(`${MAIN_URL}/profile/getempprofiledetails/${userId}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .pipe(
        map((response: any) => {
          return {
            empName: response[0].empName,
            mobile: response[0].mobile,
            email: response[0].email,
            teamName: response[0].teamName,
          } as ProfileData; 
        }),
        catchError((error) => {
          throw new Error(error.message);
        })
      );
  }

  updateAdminProfile(
    id: string | null,
    empName: string,
    mobile: number,
    email: string
  ): Observable<ProfileData> {
    return this.http
      .put<ProfileData>(`${MAIN_URL}/profile/updateadmprofiledetails`, {
        userid: id,
        empName,
        mobile,
        email,
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Profile Updated successfully!',
          });
        }),
        map((response) => {
          return {
            empName,
            mobile,
            email,
            teamName: response.teamName,
          } as ProfileData;
        }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Updating Profile!',
          });
          throw new Error(error.message);
        })
      );
  }

  updateEmployeeProfile(
    id: string | null,
    empName: string,
    mobile: number,
    email: string
  ): Observable<ProfileData> {
    return this.http
      .put<ProfileData>(`${MAIN_URL}/profile/updateempprofiledetails`, {
        userid: id,
        empName,
        mobile,
        email,
      })
      .pipe(
        tap(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Profile Updated successfully!',
          });
        }),
        map((response) => {
          return {
            empName,
            mobile,
            email,
            teamName: response.teamName,
          } as ProfileData;
        }),
        catchError((error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Error Updating Profile!',
          });
          throw new Error(error.message);
        })
      );
  }
}

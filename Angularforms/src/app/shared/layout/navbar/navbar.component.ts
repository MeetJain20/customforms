import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { logoutSuccess } from 'src/app/store/actions/login.action';
import { AuthState } from 'src/app/store/reducers/login.reducer';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  destination: string | null = '';
  role$: Observable<string | null>;
  isOpen: boolean=false;
  constructor(
    private store: Store<{ auth: AuthState }>,
    private router: Router,
    private messageService:MessageService
  ) {
    this.role$ = this.store.select((state) => state.auth.role);
  }

  ngOnInit(): void {
    this.role$.subscribe((role) => {
      if (role === 'admin') {
        this.destination = 'forms/admindashboard';
      } else if (role === 'employee') {
        this.destination = 'forms/employeedashboard';
      } else {
        this.destination = null;
      }
    });
  }

  loginroute() {
    if(!localStorage.getItem('token'))
    {
      this.router.navigateByUrl('/login');
    }
  }
  signuproute() {
    this.router.navigateByUrl('/signup');
  }

  logoutroute() {
    localStorage.removeItem('userid');
    localStorage.removeItem('role');
    localStorage.removeItem('token');
    this.store.dispatch(logoutSuccess());
    this.messageService.add({severity:'success', summary:'Success', detail:'Logout successful!'});
    this.router.navigateByUrl('/landingpage');
  }

  profileDetailEdit(){
    this.isOpen = true;
  }

  closeModal(){
    this.isOpen = false;
  }

}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ui-modal',
  templateUrl: './ui-modal.component.html',
  styleUrls: ['./ui-modal.component.scss']
})
export class UiModalComponent implements OnInit {
  constructor() { }
@Output() closemodal = new EventEmitter();
  ngOnInit(): void {
  }
  closeModal(){
    this.closemodal.emit();
  }
  stopPropagation(event: MouseEvent) {
    event.stopPropagation();
  }
}

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AuthState } from '../store/reducers/login.reducer';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store<{ auth: AuthState }>,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.pipe(
      select((state) => state.auth.isLoggedIn),
      switchMap((isLoggedIn: boolean) => {
        if (route.data['allowWhenLoggedIn'] === isLoggedIn) {
          return of(true);
        } else {
          return this.store.pipe(
            select((state) => state.auth.role),
            map((role) => {
              if (role === 'admin') {
                return this.router.createUrlTree(['/forms/admindashboard']);
              } else if (role === 'employee') {
                return this.router.createUrlTree(['/forms/employeedashboard']);
              } else {
                return this.router.parseUrl(route.data['redirectTo']);
              }
            })
          );
        }
      })
    );
  }
}

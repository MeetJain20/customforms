import { createReducer, on } from '@ngrx/store';
import { loginSuccess, loginFailure,logoutSuccess } from '../actions/login.action';

export interface AuthState {
  userId: string | null;
  role: string | null;
  isLoggedIn: boolean;
  error: string | null;
}

export const initialState: AuthState = {
  userId: null,
  role: null,
  isLoggedIn: false,
  error: null,
};

export const authReducer = createReducer(
  initialState,
  on(loginSuccess, (state, { userId, role }) => ({
    ...state,
    userId,
    role,
    isLoggedIn: true,
    error: null,
  })),
  on(loginFailure, (state, { error }) => ({
    ...state,
    error,
    isLoggedIn: false,
  })),
  on(logoutSuccess, (state) => ({
    ...state,
    userId:null,
    role: null,
    isLoggedIn: false,
  }))
);

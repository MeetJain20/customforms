import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
// import { login, login, loginFailure};
import { login,loginFailure,loginSuccess } from 'src/app/store/actions/login.action';
import { LoginService } from '../../AuthModule/services/login.service';
@Injectable()
export class LoginEffects {
  constructor(private actions$: Actions, private loginService: LoginService) {}

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      switchMap(({ email, password, role }) => {
        return this.loginService.login(email, password, role).pipe(
          map(({ userId, role }) => loginSuccess({ userId, role })),
          catchError((error) => of(loginFailure({ error: error.message })))
        );
      })
    )
  );
}

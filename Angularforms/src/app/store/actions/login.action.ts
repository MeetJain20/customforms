import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Auth] Login',
  props<{ email: string; password: string; role: string }>()
);
export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ userId: string; role: string }>()
);
export const logoutSuccess = createAction(
  '[Auth] Logout Success',
);
export const loginFailure = createAction(
  '[Auth] Login Failure',
  props<{ error: string }>()
);

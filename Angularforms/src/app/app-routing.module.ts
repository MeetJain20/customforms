import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingpageComponent } from './shared/landingpage/landingpage.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './shared/layout/navbar/navbar.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./AuthModule/auth.module').then((m) => m.AuthModule),
      canActivate: [AuthGuard],
      data: { allowWhenLoggedIn: false, redirectTo: '/forms/employeedashboard' },
  },
  {
    path: 'forms',
    loadChildren: () =>
      import('./features/FormManagementModule/forms.module').then((m) => m.FormManagementModule),
      canActivate: [AuthGuard],
      data: { allowWhenLoggedIn: true, redirectTo: '/login' },
  },
  {
    path: 'actions',
    loadChildren: () =>
      import('./features/FormManagementModule/FormAction/form-action.module').then(
        (m) => m.FormActionModule
      ),
      canActivate: [AuthGuard],
      data: { allowWhenLoggedIn: true, redirectTo: '/login' },
  },
  {
    path: 'landingpage',
    component: LandingpageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/layout/navbar/navbar.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AuthModule } from './AuthModule/auth.module';
import { LandingpageComponent } from './shared/landingpage/landingpage.component';
import { FeatureItemComponent } from './shared/landingpage/components/feature-item/feature-item.component';
import { environment } from 'src/environments/environment';
import { ButtonModule } from 'primeng/button';
import { authReducer } from './store/reducers/login.reducer';
import {DropdownModule} from 'primeng/dropdown';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsInterceptor } from './features/FormManagementModule/interceptor/forms.interceptor';
import { UiModalComponent } from './shared/ui/ui-modal/ui-modal.component';
import { ProfileComponent } from './shared/profile/profile/profile.component';
import { profileReducer } from './shared/profile/store/profile.reducer';
import { ProfileEffects } from './shared/profile/store/profile.effects';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { LoaderComponent } from './shared/Loader/loader/loader.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LandingpageComponent,
    FeatureItemComponent,
    UiModalComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ButtonModule,
    DropdownModule,
    ToastModule,
    ReactiveFormsModule,
    StoreModule.forRoot({auth:authReducer,profile: profileReducer}),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: !isDevMode(), // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
    }),
    EffectsModule.forRoot([ProfileEffects]),
  ],
  exports:[
    
  ],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FormsInterceptor,
      multi: true
    },
    MessageService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

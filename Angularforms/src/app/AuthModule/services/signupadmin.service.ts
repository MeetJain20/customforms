

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { MAIN_URL } from 'src/app/constants/constant';
import { AdminModel } from '../types/admin';

@Injectable({
  providedIn: 'root'
})
export class SignupadminService {

  constructor(private http: HttpClient) { }

  signup(empName: string, mobile: number, teamName: string, email: string, password: string): Observable<AdminModel> {
    const apiUrl = `${MAIN_URL}/admin/signupadm`;
    const requestBody = { empName, mobile, teamName, email, password };
    return this.http.post<{ user: AdminModel }>(apiUrl, requestBody, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).pipe(
      map(response => response.user)
    );
  }
}

import { TestBed } from '@angular/core/testing';

import { SignupadminService } from './signupadmin.service';

describe('SignupadminService', () => {
  let service: SignupadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignupadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

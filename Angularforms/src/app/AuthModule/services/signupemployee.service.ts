

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { MAIN_URL } from 'src/app/constants/constant';
import { EmployeeModel } from '../types/employee';
@Injectable({
  providedIn: 'root'
})
export class SignupemployeeService {

  constructor(private http: HttpClient) { }

  signup(empName: string, mobile: number, teamName: string, email: string, password: string): Observable<EmployeeModel> {
    const apiUrl = `${MAIN_URL}/employee/signupemp`;
    const requestBody = { empName, mobile, teamName, email, password };
    return this.http.post<{ user: EmployeeModel }>(apiUrl, requestBody, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).pipe(
      map(response => response.user)
    );
  }
}

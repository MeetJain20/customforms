import { TestBed } from '@angular/core/testing';

import { SignupemployeeService } from './signupemployee.service';

describe('SignupemployeeService', () => {
  let service: SignupemployeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignupemployeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

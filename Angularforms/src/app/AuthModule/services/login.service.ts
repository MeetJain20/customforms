import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MAIN_URL } from 'src/app/constants/constant';
import { LoginData } from '../types/logintype';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private messageService: MessageService
  ) { }

  login(email: string, password: string, role: string): Observable<{ userId: string, role: string }> {
    const apiUrl = `${MAIN_URL}/employee/login`;

    return this.http.post<LoginData>(apiUrl, { email, password, role }).pipe(
      map((response: any) => {
        localStorage.setItem('userid', response.id);
        localStorage.setItem('role', response.role);
        localStorage.setItem('token', response.token);
        this.messageService.add({severity:'success', summary:'Success', detail:'Login successful!'});
        if (response.role === 'admin') {
          this.router.navigate(['forms/admindashboard']);
        } else {
          this.router.navigate(['forms/employeedashboard']);
        }
        return { userId: response.id, role: response.role };
      }),
      catchError(error => {
        this.messageService.add({severity:'error', summary:'Error', detail:'Login failed. Please check your credentials or try again later.'});
        throw new Error(error);
      })
    );
  }

}

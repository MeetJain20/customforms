import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MAIN_URL } from 'src/app/constants/constant';
import { TeamNames } from '../types/teamname';

@Injectable({
  providedIn: 'root'
})
export class GetTeamsService {


  constructor(private http: HttpClient) { }

  getTeamNames(): Observable<TeamNames> {

    return this.http.get<TeamNames>(`${MAIN_URL}/employee/getteamnames`)
      .pipe(
        catchError(error => {
          throw error;
        })
      );
  }
}

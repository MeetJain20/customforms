import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { login } from '../../../store/actions/login.action';
import { AuthState } from '../../../store/reducers/login.reducer';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginFormData: FormGroup;
  error$: Observable<string | null>;

  constructor(
    private store: Store<{ auth: AuthState }>,
    private router: Router,
    private messageService:MessageService
  ) {
    this.loginFormData = new FormGroup({
      email: new FormControl('', [Validators.email,this.noEmptySpacesValidator,Validators.required]),
      password: new FormControl('', [Validators.minLength(8),this.noEmptySpacesValidator,Validators.required]),
      role: new FormControl('employee'),
    });
    this.error$ = store.select((state) => state.auth.error);
  }

  ngOnInit() {
    this.loginFormData.reset();
  }

  onSubmit() {
    if (this.loginFormData.invalid) {
      this.messageService.add({severity:'warn', summary:'Warning', detail:'Please fill in all fields!'});
      return;
    }
    const email = this.loginFormData.get('email')?.value;
    const password = this.loginFormData.get('password')?.value;
    let role = 'employee';
    if (this.loginFormData.get('role')?.value && this.loginFormData.get('role')?.value.length > 0) {
      role = 'admin';
    }
    this.store.dispatch(login({ email, password, role }));

  }

  gotosignup(){
    this.router.navigateByUrl('/signup');
  }
  noEmptySpacesValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value && control.value.trim().length === 0) {
      return { 'emptySpaces': true };
    }
    return null;
  }

}

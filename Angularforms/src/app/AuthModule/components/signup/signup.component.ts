import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { AuthState } from '../../../store/reducers/login.reducer';
import { GetTeamsService } from '../../services/get-teams.service';
import { SignupadminService } from '../../services/signupadmin.service';
import { SignupemployeeService } from '../../services/signupemployee.service';
import { OptionType } from 'src/app/features/FormManagementModule/FormAction/type/optiontype';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit,OnDestroy {
  signupFormData: FormGroup;
  error$: Observable<string | null>;
  selectedTeam!: string;
  selectedRole!: string;
  teams: string[] = [];
  hasTeam: boolean = false;
  role: OptionType[] = [
    { label: 'Admin', value: 'admin' },
    { label: 'Employee', value: 'employee' },
  ];
  subscriptions: Subscription[] = [];

  constructor(
    private store: Store<{ auth: AuthState }>,
    private router: Router,
    private teamService: GetTeamsService,
    private signupadminService: SignupadminService,
    private signupemployeeService: SignupemployeeService,
    private messageService:MessageService
  ) {
    this.signupFormData = new FormGroup(
      {
        empName: new FormControl('', [
          Validators.required,
          this.noEmptySpacesValidator,
        ]),
        mobile: new FormControl(0, [
          Validators.required,
          this.noEmptySpacesValidator,
          this.mobileLengthValidator,
        ]),
        teamName: new FormControl(''),
        teamName1: new FormControl('', this.noEmptySpacesValidator),
        email: new FormControl('', [
          Validators.email,
          this.noEmptySpacesValidator,
        ]),
        password: new FormControl('', [
          Validators.minLength(8),
          this.noEmptySpacesValidator,
        ]),
        role: new FormControl('', [Validators.required]),
      },
      { validators: this.noEmptyTeamValidator }
    );
    this.error$ = store.select((state) => state.auth.error);
  }

  ngOnInit() {
    this.signupFormData.reset();
    this.fetchTeamNames();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  onSubmit() {
    if (this.signupFormData.invalid) {
      this.messageService.add({severity:'warn', summary:'Warning', detail:'Please fill in all fields!'});
      return;
    }

    let teamName = '';
    const empName = this.signupFormData.get('empName')?.value;
    const mobile = this.signupFormData.get('mobile')?.value;
    const mobileNumber = Number(mobile);
    if (this.signupFormData.get('teamName1')?.value === '') {
      teamName = this.signupFormData.get('teamName')?.value.label;
    } else {
      teamName = this.signupFormData.get('teamName1')?.value;
    }
    const email = this.signupFormData.get('email')?.value;
    const password = this.signupFormData.get('password')?.value;
    const role = this.signupFormData.get('role')?.value.value;

    if (role === 'admin') {
      const adminSignupSub = this.signupadminService
        .signup(empName, mobileNumber, teamName, email, password)
        .subscribe(
          (user) => {
            this.router.navigate(['/login']);
          },
          (error) => {
            console.error('Signup failed:', error);
          }
        );
      this.subscriptions.push(adminSignupSub);
    } else if (role === 'employee') {
      const employeeSignupSub = this.signupemployeeService
        .signup(empName, mobileNumber, teamName, email, password)
        .subscribe(
          (user) => {
            this.router.navigate(['/login']);
          },
          (error) => {
            console.error('Signup failed:', error);
          }
        );
      this.subscriptions.push(employeeSignupSub);
    }
  }

  gotologin() {
    this.router.navigateByUrl('/login');
  }

  fetchTeamNames() {
    const fetchTeamsSub = this.teamService.getTeamNames().subscribe(
      (responseData: any) => {
        if (responseData) {
          this.hasTeam = true;
          const updatedOptionList = responseData.map((item: any) => ({
            value: item.trim().replace(/\s/g, '').toLowerCase(),
            label: item.charAt(0).toUpperCase() + item.slice(1),
          }));
          this.teams = updatedOptionList;
        } else {
          this.hasTeam = false;
        }
      },
      (error: any) => {
        console.error(error);
      }
    );
    this.subscriptions.push(fetchTeamsSub);
  }

  noEmptySpacesValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    if (control.value && control.value.trim().length === 0) {
      return { emptySpaces: true };
    }
    return null;
  }

  noEmptyTeamValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    const teamNameValue = control.get('teamName')?.value;
    const teamName1Value = control.get('teamName1')?.value;

    if (!(teamNameValue || teamName1Value)) {
      return { emptyTeam: true };
    }
    return null;
  }

  mobileLengthValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    const mobileNumber = control.value;
    if (mobileNumber && mobileNumber.toString().length !== 10) {
      return { invalidMobileLength: true };
    }
    return null;
  }
}
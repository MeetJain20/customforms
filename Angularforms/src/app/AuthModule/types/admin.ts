export interface AdminModel {
    _id: string;
    empName: string;
    mobile: number;
    email: string;
    password: string;
    teamName: string;
    role: string;
  }
  
export interface LoginData{
    id: string,
    role:string,
    token:string
}
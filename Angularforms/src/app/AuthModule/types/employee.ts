export interface EmployeeModel {
  _id: string;
  empName: string;
  mobile: number;
  email: string;
  password: string;
  teamName: string;
  role: string;
  adminId: string[];
}
